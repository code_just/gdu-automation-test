package stepdefinition;

import cucumber.api.PendingException;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import drivers.DriverInit;
import page_factory.alocar_colaborador.PFAlocarColaborador;

public class SDAlocarColaborador {

    PFAlocarColaborador alocarColaborador = new PFAlocarColaborador();

    @Entao("^valido se o usuario: \"([^\"]*)\", esta com acesso ao portal GDU$")
    public void validoSeOUsuarioFoiAlocadoComSucessoEComAcessoAoPortalGDU(String setor) throws Throwable {
        alocarColaborador.validarAcesso_GDU(setor);
    }

    @Quando("^acesso com \"([^\"]*)\" para validar acesso negado ao GDU$")
    public void acessoComParaValidarAcessoNegadoAoGDU(String setor) throws Throwable {
        alocarColaborador.validarAcessoNegado_GDU(setor);
    }

    @E("^realizo os procedimentos com o sysadmin de alocar colaborador com as informacoes: \"([^\"]*)\", \"([^\"]*)\"$")
    public void realizoOsProcedimentosComOSysadminDeAlocarColaboradorComAsInformacoes(String setor, String usuario) throws Throwable {
        alocarColaborador.alocarColaborador(setor, usuario);
    }
}
