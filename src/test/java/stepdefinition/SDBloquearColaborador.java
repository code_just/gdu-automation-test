package stepdefinition;

import cucumber.api.PendingException;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import page_factory.bloquear_colaborador.PFBloquearColaborador;

public class SDBloquearColaborador {

    PFBloquearColaborador bloquearColaborador = new PFBloquearColaborador();

    @E("^acesso a tela de bloquear colaborador com o sysadmin$")
    public void acessoATelaDeBloquearColaboradorComOSysadmin() throws InterruptedException {
        bloquearColaborador.acessarTelaBloqueioComSysadmin();
    }

    @E("^seleciono o colaborador da Linha: \"([^\"]*)\", setor: \"([^\"]*)\" para \"([^\"]*)\" e agendo para hoje$")
    public void selecionoOColaboradorDaLinhaSetorParaEAgendoParaHoje(String linha, String setor, String tipoBloqueio) throws Throwable {
        bloquearColaborador.bloquear_afastarColaborador(linha, setor, tipoBloqueio);
    }

    @Quando("^valido se o usuario: \"([^\"]*)\", esta com acesso ao portal GDU para conferir bloqueio$")
    public void validoSeOUsuarioEstaComAcessoAoPortalGDUParaConferirBloqueio(String usuario) throws Throwable {
        bloquearColaborador.validarAcesso_GDU(usuario);
    }

    @Entao("^acesso com \"([^\"]*)\" para validar acesso negado ao GDU para conferir bloqueio$")
    public void acessoComParaValidarAcessoNegadoAoGDUParaConferirBloqueio(String setor) throws Throwable {
        bloquearColaborador.validarAcessoNegado(setor);
    }

    @E("^confirmo \"([^\"]*)\" antes do horario marcado$")
    public void confirmoAntesDoHorarioMarcado(String tipoBloqueio) throws Throwable {
        bloquearColaborador.forcarDesligamentoAfastamento();
    }
}
