package stepdefinition;

import cucumber.api.PendingException;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import page_factory.ComunsElements.PFComunsElements;
import page_factory.PFLogin;

import java.sql.SQLException;

public class SDLogin {
    PFLogin login = new PFLogin();
    PFComunsElements comunsElements = new PFComunsElements();

    @Quando("^insiro o usuario: \"([^\"]*)\"$")
    public void insiroOUsuario(String usuario) throws Throwable {
        login.acessarPortalEfetivdade(usuario);
    }

    @E("^acesso o portal GDU$")
    public void acessoOPortalGDU() throws InterruptedException {
        login.acessarPortalGDU();
    }

    @E("^acesso a tela: \"([^\"]*)\"$")
    public void acessoATela(String nomeTela) throws Throwable {
        login.acessarTela_GDU(nomeTela);
    }

    @Entao("^valido se a mensagem: \"([^\"]*)\" foi exibida com sucesso$")
    public void validoSeAMensagemFoiExibidaComSucesso(String msgEsperada) throws Throwable {
        comunsElements.validarMensagemGDU(msgEsperada);
    }

//    @Dado("^acesso o banco de dados$")
//    public void acessoOBancoDeDados() throws SQLException, ClassNotFoundException {
//        login.acessarMysql();
//    }
}
