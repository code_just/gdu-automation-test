package stepdefinition;

import cucumber.api.PendingException;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import drivers.DriverInit;
import page_factory.Empresas.PFEmpresas;

public class SDEmpresas {

    private PFEmpresas empresas = new PFEmpresas();

    @E("^edito os campos da tela empresa: \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\"$")
    public void editoOsCamposDaTelaEmpresaParaCriacao(String codigoEmpresa, String nomeEmpresa, boolean empresaAtiva) throws Throwable {
        empresas.criarEmpresa(codigoEmpresa, nomeEmpresa, empresaAtiva);
    }

    @E("^pesquiso pela empresa: \"([^\"]*)\"$")
    public void pesquisoPelaEmpresa(String pesquisarEmpresa) throws Throwable {
        empresas.pesquisarEmpresa(pesquisarEmpresa);
    }

    @E("^edito os campos da tela empresa: \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", para edicao$")
    public void editoOsCamposDaTelaEmpresaParaEdicao(String codigoEmpresa, String nomeEmpresa, boolean empresaAtiva) throws Throwable {
        empresas.editarEmpresa(codigoEmpresa, nomeEmpresa, empresaAtiva);
    }

    @Entao("^valido se a empresa foi inativada com sucesso$")
    public void validoSeAEmpresaEstaInativadaComSucesso() throws Throwable {
        empresas.validarEmpresaInativa();
    }

    @Entao("^valido se a empresa foi ativada com sucesso$")
    public void validoSeAEmpresaFoiAtivadaComSucesso() {
        empresas.validarEmpresaAtiva();
    }
}
