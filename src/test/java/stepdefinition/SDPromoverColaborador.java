package stepdefinition;

import cucumber.api.PendingException;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import page_factory.promover_colaborador.PFPromoverColaborador;

public class SDPromoverColaborador {

    private PFPromoverColaborador promoverColaborador = new PFPromoverColaborador();

    @Entao("^valido quais linhas o colaborador pertence e se no campo linha atual exbixe somente as linhas esperadas$")
    public void validoQuaisLinhasOColaboradorPertenceESeNoCampoLinhaAtualExbixeSomenteAsLinhasEsperadas() {

    }

    @Entao("^que o setor no campo setor novo seja sempre acima do setor atual$")
    public void queOSetorNoCampoSetorNovoSejaSempreAcimaDoSetorAtual() {
    }

    @Entao("^valido que o usuario: \"([^\"]*)\" so pode ter acesso as linhas: \"([^\"]*)\"$")
    public void validoQueOUsuarioSoPodeTerAcessoAsLinhas(String usuario, String linhas) throws Throwable {
        promoverColaborador.validarPermisaoDeLinhas(linhas);
    }

    @E("^insiro as informacoes nos campos: \"([^\"]*)\", \"([^\"]*)\" para: \"([^\"]*)\" e \"([^\"]*)\"$")
    public void insiroAsInformacoesNosCamposParaE(String linhaAtual, String setorAtual, String linhaNova, String setorNovo) throws Throwable {
        promoverColaborador.realizarPromocao(linhaAtual, setorAtual, linhaNova, setorNovo);
    }
}
