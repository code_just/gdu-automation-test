package stepdefinition;

import cucumber.api.PendingException;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import page_factory.PFStepByStep;
import page_factory.Telas_StepByStep.PFContatos;
import page_factory.Telas_StepByStep.PFDadosPessoais;
import page_factory.Telas_StepByStep.PFEnderecos;

public class SDStepByStep {
    PFStepByStep stepByStep = new PFStepByStep();
    PFDadosPessoais dadosPessoais = new PFDadosPessoais();
    PFEnderecos enderecos = new PFEnderecos();
    PFContatos contatos = new PFContatos();


    @Entao("^valido se o icone de step by step esta sendo exibido com sucesso$")
    public void validoSeOIconeStepyByStepEstaSendoExibidaComSucesso() throws InterruptedException {
        stepByStep.validarPrimeiroAcesso();
    }

    @Entao("^valido se o icone de step by step nao esta sendo exibido com sucesso$")
    public void validoSeOIconeDeStepByStepNaoEstaSendoExibidoComSucesso() {
        stepByStep.validarQueUsarioJaAcessouGDU();
    }

    @Entao("^deparar se os campos: \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\" e \"([^\"]*)\" estao com as informacoes esperadas e desabiltados para edicao$")
    public void depararSeOsCamposEEstaoComAsInformacoesEsperadasEDesabiltadosParaEdicao(String nome, String matricula, String cpf, String idRede) throws Throwable {
        dadosPessoais.depararCamposNomeMatriculaCPFIdRede_EDesabilitados(nome, matricula, cpf, idRede);
    }

    @Entao("^valido se a mensagem salvo com sucesso foi exibida com sucesso$")
    public void validoSeAMensagemDeSucessoFoiExibidaComSucesso() {
        dadosPessoais.validarMensagemDeSucessoExibida();
    }

    @E("^edito os campos: \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\"$")
    public void editoOsCampos(String dataNasc, String apelido, String estadoCivil, String sexo, String numeroCamisa, boolean fumante, boolean pcd) throws Throwable {
        dadosPessoais.preencherCampos(dataNasc, apelido, estadoCivil, sexo, numeroCamisa, fumante, pcd);
    }

    @E("^edito os campos: \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\"$")
    public void editoOsCampos(String cep, String logradouro, String bairro, String cidade, String estado, String numero, String complemento, boolean residencial, boolean amostra) throws Throwable {
        enderecos.preencherCampos_Endereco(cep, logradouro, bairro, cidade, estado, numero, complemento, residencial, amostra);
    }

    @E("^acesso a aba: Meus Endereços$")
    public void acessoAAbaMeusEndereços() {
        enderecos.acessarAba_MeusEnderecos();
    }

    @E("^clico no primeiro endereco cadastrado$")
    public void clicoNoPrimeiroEnderecoCadastrado() {
        enderecos.clicarPrimeiroEnderecoCadastrado();
    }

    @E("^verifico se o botao avancar esta desabilitado$")
    public void verificoSeOBotaoAvancarEstaDesabilitado() {
        enderecos.validarSeBotaoAvancarEstaDesabilitado();
    }

    @Entao("^valido se o botao avancar ficou habilitado com sucesso$")
    public void validoSeOBotaoAvancarFicouHabilitadoComSucesso() {
        enderecos.validarSeBotaoAvancarEstaHabilitado();
    }

    @Entao("^verifico se existe um endereco ja com a opcao residencial marcada$")
    public void verificoSeExisteUmEnderecoJaComAOpcaoResidencialMarcada() {
        enderecos.verificarSePrimeiroEnderecoCadastradoEhResidencial();
    }

    @E("^acesso a aba: Cadastrar$")
    public void acessoAAbaCadastrar() {
        enderecos.acessarAba_Cadastrar();
    }

    @Entao("^valido se o novo endereco criado ficou com a opcao marcada e o endereco antigo sem a opcao residencial marcada$")
    public void validoSeONovoEnderecoCriadoFicouComAOpcaoMarcadaEOEnderecoAntigoSemAOpcaoResidencialMarcada() {
        enderecos.validarEnderecoResidencialFoiSobrescrito();
    }

    @E("^acesso a aba: Telefone$")
    public void acessoAAbaTelefone() {
        contatos.acessarAba_Telefones();

    }

    @E("^insiro nos campos de telefone com as informacoes: \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", para edicao$")
    public void editoOsCampos(String tipoTelefone, String numeroTelefone, String observacaoTelefone) throws Throwable {
        contatos.editarNumeroCadastrado(tipoTelefone, numeroTelefone, observacaoTelefone);
    }

    @E("^insiro nos campos de telefone com as informacoes: \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", para criacao$")
    public void insiroNosCamposDeTelefoneComAsInformacoesParaCriacao(String tipoTelefone, String numeroTelefone, String observacaoTelefone) throws Throwable {
        contatos.cadastrarNumeroNovo(tipoTelefone, numeroTelefone, observacaoTelefone);
    }

    @E("^acesso a aba: Email$")
    public void acessoAAbaEmail() {
        contatos.acessarAba_Email();
    }

    @E("^insiro nos campos de email com as informacoes: \"([^\"]*)\", \"([^\"]*)\", para criacao$")
    public void insiroNosCamposDeEmailComAsInformacoesParaCriacao(String email, String observacao) throws Throwable {
        contatos.cadastrarEmailNovo(email, observacao);
    }

    @E("^insiro nos campos de email com as informacoes: \"([^\"]*)\", \"([^\"]*)\", para edicao$")
    public void insiroNosCamposDeEmailComAsInformacoesParaEdicao(String email, String observacao) throws Throwable {
        contatos.editarEmailCadastrado(email, observacao);
    }
}
