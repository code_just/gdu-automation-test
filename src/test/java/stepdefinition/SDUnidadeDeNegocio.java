package stepdefinition;

import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import page_factory.BUs.PFBUs;

public class SDUnidadeDeNegocio {

    PFBUs pfbUs = new PFBUs();

    @E("^edito os campos da tela de BU: \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\"$")
    public void editoOsCamposDaTelaDeBU(String codigoBU, String nomeBu, String nomeEmpresa, String tipoBu, boolean buAtivo) throws Throwable {
        pfbUs.criarBUComSucesso(codigoBU, nomeBu, nomeEmpresa, tipoBu, buAtivo);
    }

    @E("^pesquiso pelo BU: \"([^\"]*)\"$")
    public void pesquisoPeloBU(String bu) throws Throwable {
        pfbUs.pesquisarBU(bu);
    }

    @E("^edito os campos da tela de BU: \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", para edicao$")
    public void editoOsCamposDaTelaDeBUParaEdicao(String nomeBu, String nomeEmpresa, String tipoBu, boolean buAtivo) throws Throwable {
        pfbUs.editarBUComSucesso(nomeBu, nomeEmpresa, tipoBu, buAtivo);
    }

    @Entao("^valido se o BU foi inativado com sucesso$")
    public void validoSeOBUFoiInativadoComSucesso() {
        pfbUs.validarBUInativo();
    }

    @Entao("^valido se o BU foi ativado com sucesso$")
    public void validoSeOBUFoiAtivadoComSucesso() {
        pfbUs.validarBUAtivo();
    }

    @Entao("^valido se as empresas inativadas nao estao sendo exibidas no campo select da tela BU$")
    public void validoSeAsEmpresasInativadasNaoEstaoSendoExibidasNoCampoSelectDaTelaBU() {
        pfbUs.validarSeEmpresasInativasSãoExibidas();
    }
}
