package drivers;

import dataProviders.ConfigFileReader;
import managers.FileReaderManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

public class DriverInit {

    public static WebDriver driver = null;

    public DriverInit(){

    }

    public void inicializarDriver(String navegador){
        if (navegador.toLowerCase().equals("chrome") && driver == null){
            System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"/src/test/java/web_drivers/chromedriver.exe");
            LoggingPreferences loggingprefs = new LoggingPreferences();
            loggingprefs.enable(LogType.BROWSER, Level.WARNING);
            loggingprefs.enable(LogType.PERFORMANCE, Level.WARNING);

            DesiredCapabilities capabilities = DesiredCapabilities.chrome();
            capabilities.setCapability(CapabilityType.LOGGING_PREFS, loggingprefs);

            ChromeOptions options = new ChromeOptions();
            options.addArguments("--disable-dev-shm-usage");
            options.addArguments("--ignore-certificate-errors");
            options.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
            options.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
            capabilities.setCapability(ChromeOptions.CAPABILITY, options);
            driver = new ChromeDriver(capabilities);
            driver.manage().deleteAllCookies();
            driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
            driver.manage().window().maximize();
        }else if (navegador.toLowerCase().equals("firefox")){
            System.setProperty("webdriver.gecko.driver", "C:/Users/Lucas/Documents/Automacao/FrameworkAutomacao/FrameworkAutomacao/src/test/java/web_drivers/geckodriver.exe");
            DesiredCapabilities capabilities= DesiredCapabilities.firefox();
            capabilities.setCapability("marionette", true);
            driver = new FirefoxDriver(capabilities);
            driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
            driver.manage().window().maximize();
        }
    }

    public static WebDriver getBrowser(){
        if (driver == null){
            return driver;
        }
        else{
            return driver;
        }
    }

    public void acessarPagina(String url){
        getBrowser().get(url);
    }

    public static void closeBrowser() throws Exception{
        if (driver == null) {
            return;
        }
        driver.quit();
        driver = null;
    }
}
