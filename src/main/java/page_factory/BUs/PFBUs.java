package page_factory.BUs;

import gherkin.lexer.Sr_cyrl;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page_factory.ComunsElements.PFComunsElements;
import page_factory.Empresas.PFEmpresas;
import report.ReportCucumber;

import java.util.List;

import static drivers.DriverInit.getBrowser;
import static methods.Methods.*;

public class PFBUs {

    PFComunsElements comunsElements = new PFComunsElements();
    PFEmpresas empresas = new PFEmpresas();

    public PFBUs(){
        PageFactory.initElements(getBrowser(), this);
    }

    @FindBy(css = "input[type='number']")
    private WebElement editavelCodigo;

    @FindBy(css = "[placeholder='Nome']")
    private WebElement editavelNome;

    @FindBy(css = "tr[mode='add'] > td:nth-of-type(4) .MuiInputBase-root")
    private WebElement botaoSelectEmpresa;

    @FindBy(css = "tr[data='[object Object]'] > td:nth-of-type(4) .MuiInputBase-root")
    private WebElement botaoSelectEmpresaEditar;

    @FindBy(css = "tr[mode='add'] > td:nth-of-type(5) .MuiInputBase-root")
    private WebElement botaoSelectTipo;

    @FindBy(css = "tr[data='[object Object]'] > td:nth-of-type(5) .MuiInputBase-root")
    private WebElement botaoSelectTipoEditar;

    @FindBy(css = "tr[mode='add'] > td:nth-of-type(6) > span:nth-child(1)")
    private WebElement botaoAtivarInativarBU;

    @FindBy(css = "tr[data='[object Object]'] > td:nth-of-type(6) > span:nth-child(1)")
    private WebElement botaoAtivarInativarBUEditar;

    @FindBy(xpath = "(//span[.='check'])[2]")
    private WebElement botaoSalvar;

    @FindBy(xpath = "(//span[.='add_box'])[3]")
    private WebElement botaoNovoBU;

    @FindBy(css = "[placeholder='Pesquisar unidade']")
    private WebElement editavelPesquisar;

    @FindBy(css = "button[title='Editar unidade']")
    private WebElement botaoEditarBu;

    @FindBy(css = ".MuiTableBody-root td:nth-child(2) > p")
    private WebElement campoValidarInativo;

    @FindBy(xpath = "//span[.='person_addUnidades de negócio']")
    private WebElement botaoTelaUnidadesDeNegocio;


    public void criarBUComSucesso(String codigoBU, String nomeBu, String nomeEmpresa, String tipoBu, boolean buAtivo){
        aguardar(2);
        clicarElemento(botaoNovoBU);

        inserirTexto_GDU(editavelCodigo, codigoBU);
        inserirTexto_GDU(editavelNome, nomeBu);
        selecionarTextoSelect_GDU(botaoSelectEmpresa, comunsElements.listaSelectGDU, nomeEmpresa);
        selecionarTextoSelect_GDU(botaoSelectTipo, comunsElements.listaSelectGDU, tipoBu);
        ativar_inativarBU(buAtivo);

        clicarElemento(botaoSalvar);
    }

    public void editarBUComSucesso(String nomeBu, String nomeEmpresa, String tipoBu, boolean buAtivo){
        aguardar(2);
        clicarElemento(botaoEditarBu);

        inserirTexto_GDU(editavelNome, nomeBu);
        selecionarTextoSelect_GDU(botaoSelectEmpresaEditar, comunsElements.listaSelectGDU, nomeEmpresa);
        selecionarTextoSelect_GDU(botaoSelectTipoEditar, comunsElements.listaSelectGDU, tipoBu);
        ativar_inativarBUEditar(buAtivo);

        clicarElemento(botaoSalvar);
    }

    public void pesquisarBU(String bu){
        inserirTexto_GDU(editavelPesquisar, bu);
        ReportCucumber.report("BU pesquisado com sucesso.");
    }

    private void ativar_inativarBU(boolean buAtivo){
        if (!obterAtributoDoElemento(botaoAtivarInativarBU, "class").contains("Mui-checked")
                && buAtivo){
            clicarElemento(botaoAtivarInativarBU);
            ReportCucumber.report("BU ativado com sucesso.");
        }

        if (obterAtributoDoElemento(botaoAtivarInativarBU, "class").contains("Mui-checked")
                && !buAtivo){
            clicarElemento(botaoAtivarInativarBU);
            ReportCucumber.report("BU desativado com sucesso");
        }
    }

    public void validarSeEmpresasInativasSãoExibidas(){
       List<String> empresasInativas = empresas.obterEmpresasInativas();

        acessarTela_UnidadesDeNegocio();
        aguardar(2);
        clicarElemento(botaoNovoBU);
        clicarElemento(botaoSelectEmpresa);


        for (WebElement element : comunsElements.listaSelectGDU){
            if (empresasInativas.contains(obterTexto(element))){
                ReportCucumber.report(getBrowser(), "Uma empresa inativada esta aparecendo no select. - FALHA", true);
                Assert.fail("Uma empresa inativada esta aparecendo no select. - FALHA");
            }
        }
    }

    private void ativar_inativarBUEditar(boolean buAtivo){
        if (!obterAtributoDoElemento(botaoAtivarInativarBUEditar, "class").contains("Mui-checked")
                && buAtivo){
            clicarElemento(botaoAtivarInativarBUEditar);
            ReportCucumber.report("BU ativado com sucesso.");
        }

        if (obterAtributoDoElemento(botaoAtivarInativarBUEditar, "class").contains("Mui-checked")
                && !buAtivo){
            clicarElemento(botaoAtivarInativarBUEditar);
            ReportCucumber.report("BU desativado com sucesso");
        }
    }

    public void validarBUInativo(){
        if (obterValorCSSDoElemento(campoValidarInativo, "color")
        .equals("rgba(255, 0, 0, 1)")){
            ReportCucumber.report(getBrowser(), "BU esta inativo. Validacao realizada com sucesso.", true);
        }else {
            ReportCucumber.report(getBrowser(), "BU esta ativo. - FALHA.\n Esperado que o BU esteja inativo.", true);
            Assert.fail("BU esta ativo. - FALHA.\n Esperado que o BU esteja inativo.");
        }
    }

    public void validarBUAtivo(){
        if (obterValorCSSDoElemento(campoValidarInativo, "color")
                .equals("rgba(0, 0, 0, 1)")){
            ReportCucumber.report(getBrowser(), "BU esta ativo. Validacao realizada com sucesso.", true);
        }else {
            ReportCucumber.report(getBrowser(), "BU esta ativo. - FALHA.\n Esperado que o BU esteja inativo.", true);
            Assert.fail("BU esta ativo. - FALHA.\n Esperado que o BU esteja inativo.");
        }
    }

    private void acessarTela_UnidadesDeNegocio(){
        clicarElemento(botaoTelaUnidadesDeNegocio);
        ReportCucumber.report("Tela: Unidades de negocio acessado com sucesso.");
    }

}
