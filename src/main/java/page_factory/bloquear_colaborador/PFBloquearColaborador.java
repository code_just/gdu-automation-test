package page_factory.bloquear_colaborador;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page_factory.ComunsElements.PFComunsElements;
import page_factory.Empresas.PFEmpresas;
import page_factory.PFLogin;
import report.ReportCucumber;

import static drivers.DriverInit.getBrowser;
import static methods.Methods.*;
import static methods.Methods.validarSeOWebElementEExibido;

public class PFBloquearColaborador {

    private PFComunsElements comunsElements = new PFComunsElements();
    private PFLogin login = new PFLogin();

    public PFBloquearColaborador(){
        PageFactory.initElements(getBrowser(), this);
    }

    @FindBy(xpath = "//span[.='blockBloquear colaborador']")
    private WebElement botaoTelaBloquearColaborador;

    @FindBy(css = "div[itemid='businessLine'] .MuiInputBase-root")
    private WebElement botaoLinha;

    @FindBy(css = "div[itemid='businessLineUser'] .MuiInputBase-root")
    private WebElement botaoSetor;

    @FindBy(xpath = "//span[.='Desligamento']")
    private WebElement botaoDesligamento;

    @FindBy(xpath = "//span[.='Afastamento']")
    private WebElement botaoAfastamento;

    @FindBy(css = "div.MuiGrid-direction-xs-column > .MuiGrid-root div div")
    private WebElement editavelData;

    @FindBy(css = ".MuiPickersDay-current")
    private WebElement botaoCurrentData;

    @FindBy(xpath = "//span[.='OK']")
    private WebElement botaoOKData;

    @FindBy(xpath = "//span[.='Bloquear']")
    private WebElement botaoBloquear;

    @FindBy(css = ".MuiButton-outlinedSecondary")
    private WebElement botaoConfirmarDesligamentoAfastamento;

    @FindBy(xpath = "//span[.='Confirmar']")
    private WebElement botaoConfirmar;

    public void acessarTelaBloqueioComSysadmin() throws InterruptedException {
        login.realizar_Logoff();
        login.acessarPortalEfetivdade("ems52346");
        login.acessarPortalGDU();

        login.acessarTela_GDU("Bloquear colaborador");
    }

    public void bloquear_afastarColaborador(String nomeLinha, String numeroSetor, String tipoBloqueio){
        selecionarTextoSelect_GDU(botaoLinha, comunsElements.listaSelectGDU, nomeLinha);
        selecionarTextoSelect_GDU(botaoSetor, comunsElements.listaSelectGDU, numeroSetor);
        selecionarTipo(tipoBloqueio);
        selecionarCurrentData();
        clicarElemento(botaoBloquear);
        clicarElemento(botaoConfirmar);
    }

    public void forcarDesligamentoAfastamento(){
        clicarElemento(botaoConfirmarDesligamentoAfastamento);
        clicarElemento(botaoConfirmar);
    }

    public void validarAcesso_GDU(String setor) throws InterruptedException {
        login.acessarPortalEfetivdade(setor);
        login.acessarPortalGDU();

        if (validarSeOWebElementEExibido(comunsElements.botaoOpcoes)
                || validarSeOWebElementEExibido(comunsElements.linhaStepByStep)){
            ReportCucumber.report(getBrowser(), "Usuario com acesso ao portal GDU", true);
        }else {
            ReportCucumber.report(getBrowser(), "Usuario esta sem acesso ao portal GDU. - FALHA\n" +
                    "Esperado que tenha acesso ao portal GDU", true);
            Assert.fail("Usuario esta sem acesso ao portal GDU. - FALHA\n" +
                    "Esperado que usuario logado tenha acesso ao portal GDU.");
        }
    }

    public void validarAcessoNegado(String setor) throws InterruptedException {
        login.realizar_Logoff();
        login.acessarPortalEfetivdade(setor);
        login.acessarPortalGDU();

        if (!validarSeOWebElementEExibido(comunsElements.botaoOpcoes )
                && !validarSeOWebElementEExibido(comunsElements.linhaStepByStep)) {
            ReportCucumber.report(getBrowser(), "Usuario sem acesso ao portal GDU", true);
        } else {
            ReportCucumber.report(getBrowser(), "Usuario esta com acesso ao portal GDU. - FALHA\n" +
                    "Esperado que usuario logado nao tenha acesso ao portal.", true);
            Assert.fail("Usuario esta com acesso ao portal GDU. - FALHA\n" +
                    "Esperado que usuario logado nao tenha acesso ao portal.");
        }
    }

    private void selecionarCurrentData(){
        clicarElemento(editavelData);
        clicarElemento(botaoCurrentData);
        clicarElemento(botaoOKData);
    }

    private void selecionarTipo(String tipoBloqueio){
        if (tipoBloqueio.toLowerCase().equals("desligamento")){
            clicarElemento(botaoDesligamento);
        }else if (tipoBloqueio.toLowerCase().equals("afastamento")){
            clicarElemento(botaoAfastamento);
        }else {
            ReportCucumber.report("Tipo de bloqueio não encontrado. Escreva uma das opcoes: Afastamento - Desligamento");
            Assert.fail("Tipo de bloqueio não encontrado. Escreva uma das opcoes: Afastamento - Desligamento");
        }
    }

    private void acessarTela_bloquearColaborador(){
        clicarElemento(botaoTelaBloquearColaborador);
    }
}
