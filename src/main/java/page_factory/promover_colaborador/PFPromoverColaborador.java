package page_factory.promover_colaborador;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page_factory.ComunsElements.PFComunsElements;
import report.ReportCucumber;

import java.util.ArrayList;
import java.util.List;

import static drivers.DriverInit.getBrowser;
import static methods.Methods.*;

public class PFPromoverColaborador {

    public PFPromoverColaborador(){
        PageFactory.initElements(getBrowser(), this);
    }

    private PFComunsElements comunsElements = new PFComunsElements();

    @FindBy(css = "div.MuiGrid-spacing-xs-3 > div:nth-of-type(1) > div:nth-of-type(1) button:nth-of-type(2) svg:nth-of-type(1)")
    private WebElement botaoListarLinhas;

    @FindBy(css = "div.MuiGrid-spacing-xs-3 > div:nth-of-type(1) > div:nth-of-type(1) button:nth-of-type(2) svg:nth-of-type(1)")
    private WebElement botaoLinhaAtual;

    @FindBy(css = "div.MuiGrid-spacing-xs-3 > div:nth-of-type(1) > div:nth-of-type(2) button:nth-of-type(2) svg:nth-of-type(1)")
    private WebElement botaoSetorAtual;

    @FindBy(css = ".MuiGrid-grid-md-3.MuiGrid-container > div:nth-of-type(1) button:nth-of-type(2) svg:nth-of-type(1)")
    private WebElement botaoLinhaAserPromovido;

    @FindBy(css = "div[itemid='sectionsID'] .MuiAutocomplete-popupIndicator .MuiSvgIcon-root")
    private WebElement botaoSetorAserPromovido;

    @FindBy(xpath = "//span[.='Adicionar']")
    private WebElement botaoAdicionar;

    @FindBy(css = "div[format='DD/MM/YYYY'] > .MuiInputBase-root")
    private WebElement editavelData;

    @FindBy(id = "businessLineUser")
    private WebElement editavelSetorAtual;

    @FindBy(css = ".MuiAutocomplete-option")
    private WebElement listaSetores;

    @FindBy(id = "sectionsID")
    private WebElement editavelNovoSetor;

    @FindBy(xpath = "//span[.='OK']")
    private WebElement botaoOKData;

    @FindBy(xpath = "//span[.='Confirmar']")
    private WebElement botaoConfirmar;


    public void realizarPromocao(String linhaAtual, String setorAtual, String linhaNova, String setorNovo) throws InterruptedException {
        aguardar(2);
        selecionarTextoSelect_GDU(botaoLinhaAtual, comunsElements.listaSelectGDU, linhaAtual);
        clicarElemento(botaoSetorAtual);
        inserirTexto(editavelSetorAtual, setorAtual);
        clicarElemento(listaSetores);


        selecionarTextoSelect_GDU(botaoLinhaAserPromovido, comunsElements.listaSelectGDU, linhaNova);
        aguardar(3);
        clicarElemento(botaoSetorAserPromovido);
        inserirTexto(editavelNovoSetor, setorNovo);
        clicarElemento(listaSetores);

        clicarElemento(botaoAdicionar);
        clicarElemento(editavelData);
        clicarElemento(botaoOKData);
        clicarElemento(botaoConfirmar);
    }

    public void validarPermisaoDeLinhas(String linhas){
        String[] textoSeparado = linhas.replaceAll(" ", "").split(",");
        List<String> linhasObtidas = obterLinhas();

        int tamanho = textoSeparado.length;
        int tamanho2 = linhasObtidas.size();

        if (tamanho == tamanho2){
            ReportCucumber.report("O numero de linhas selecionaveis é o mesmo que o esperado. Esperado: " + tamanho);
        }else {
            ReportCucumber.report("O numero de linhas selecionaveis não é o mesmo que o esperado. Esperado: " + tamanho);
            Assert.fail("O numero te linhas selecionaveis não é o mesmo que o esperado. Esperado: " + tamanho);
        }

         for (String texto : textoSeparado ){
             if (linhasObtidas.contains(texto)){
                 ReportCucumber.report("Linha esperada: "+ texto+ " foi encontrada com sucesso.");
             }else {
                 ReportCucumber.report("Linha esperada: "+ texto+ " não foi encontrada com sucesso.");
                 Assert.fail("Linha esperada: "+ texto+ " não foi encontrada com sucesso.");
             }
         }
    }

    public List<String> obterLinhas(){

        List<String> linhasAtivas = new ArrayList<String>();

        clicarElemento(botaoListarLinhas);
        for (WebElement element : comunsElements.listaSelectGDU) {
            linhasAtivas.add(obterTexto(element).replaceAll(" ", ""));
        }
        return linhasAtivas;
    }
}
