package page_factory;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page_factory.ComunsElements.PFComunsElements;
import report.ReportCucumber;

import static drivers.DriverInit.getBrowser;
import static methods.Methods.acessar_iFrame;
import static methods.Methods.validarSeOWebElementEExibido;


public class PFStepByStep {

    PFComunsElements comunsElements = new PFComunsElements();

    public PFStepByStep(){
        PageFactory.initElements(getBrowser(), this);
    }

    @FindBy(css = ".MuiStepper-horizontal")
    private WebElement linhaStepByStep;

    public void validarPrimeiroAcesso() throws InterruptedException {
        if (validarSeOWebElementEExibido(linhaStepByStep)){
            ReportCucumber.report(getBrowser(), "Linha de progressao de cadastro de dados pessoais exibido com sucesso." +
                    "\nPrimeiro acesso validado com sucesso", true);
        }else{
            ReportCucumber.report(getBrowser(),"Linha de progressao de cadastro pessoal nao foi encontrada." +
                    "\nUsuario inserido provavelmente ja acessou o portal GDU. - FALHA", true);
            Assert.fail("Linha de progressao de cadastro pessoal nao foi encontrada." +
                    "\nUsuario inserido provavelmente ja acessou o portal GDU. - FALHA");
        }
    }

    public void validarQueUsarioJaAcessouGDU(){
        if (!validarSeOWebElementEExibido(linhaStepByStep)){
            ReportCucumber.report(getBrowser(), "Linha de progressao de cadastrto nao foi encontrada. Usuario já acessou" +
                    "o GDU com sucesso.", true);
        }else{
            ReportCucumber.report(getBrowser(),"Linha de progressao de cadastro pessoal encontrada." +
                    "\nUsuario inserido nunca acessou o portal GDU. - FALHA", true);
            Assert.fail("Linha de progressao de cadastro pessoal encontrada." +
                    "\nUsuario inserido nunca acessou o portal GDU. - FALHA");
        }
    }
}
