package page_factory.ComunsElements;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import report.ReportCucumber;

import java.util.List;

import static drivers.DriverInit.getBrowser;
import static methods.Methods.*;

public class PFComunsElements {

    public PFComunsElements(){
        PageFactory.initElements(getBrowser(), this);
    }

    @FindBy(id = "frmFerramenta")
    public WebElement iFrameFerramenta;

    @FindBy(id = "frame")
    public WebElement iFrameGDU;

    @FindBy(css = "ul li")
    public List<WebElement> listaSelectGDU;

    @FindBy(id = "client-snackbar")
    public WebElement spamMsgUsuario;

    @FindBy(css = ".MuiStepper-horizontal")
    public WebElement linhaStepByStep;

    @FindBy(css = ".MuiTabs-flexContainer")
    public WebElement botaoOpcoes;

    public void acessarIframesGDU(){
        acessar_iFrame(iFrameFerramenta);
        acessar_iFrame(iFrameGDU);
    }

    public void validarMensagemGDU(String msgEsperada){
        aguardar(1);
        if (obterTexto(spamMsgUsuario).contains(msgEsperada)){
            ReportCucumber.report(getBrowser(), "Mensagem: ("+msgEsperada+") exibida com sucesso.", true);
        }else{
            ReportCucumber.report(getBrowser(), "Mensagem esperada nao encontrada. - FALHA\nEsperado: "+msgEsperada+" \n Encontrado: " +obterTexto(spamMsgUsuario) , true);
            Assert.fail("Mensagem esperada nao encontrada. - FALHA\nEsperado: "+msgEsperada+" \n Encontrado: " +obterTexto(spamMsgUsuario));
        }
    }
}
