package page_factory.alocar_colaborador;


import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page_factory.ComunsElements.PFComunsElements;
import page_factory.PFLogin;
import report.ReportCucumber;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

import static drivers.DriverInit.getBrowser;
import static io.restassured.RestAssured.basic;
import static io.restassured.RestAssured.given;
import static methods.Methods.*;
import static org.hamcrest.Matchers.hasKey;

public class PFAlocarColaborador {

    PFComunsElements comunsElements = new PFComunsElements();
    PFLogin login = new PFLogin();

    public PFAlocarColaborador(){
        PageFactory.initElements(getBrowser(), this);
    }


    @FindBy(id = "sections-select")
    private WebElement botaoSetor;

    @FindBy(id = "network-user-input")
    private WebElement editavelUsuario;

    @FindBy(xpath = "//span[.='Salvar']")
    private WebElement botaoSalvar;

    public void alocarColaborador(String setor, String usuario) throws InterruptedException {
        login.realizar_Logoff();
        login.acessarPortalEfetivdade("ems52346");
        login.acessarPortalGDU();

        login.acessarTela_GDU("Alocar colaborador");

        inserirTexto_GDU(editavelUsuario, usuario);
        selecionarTextoSelect_GDU(botaoSetor, comunsElements.listaSelectGDU, setor);
        clicarElemento(botaoSalvar);
    }

    public void validarAcessoNegado_GDU(String setor) throws InterruptedException, IOException {
        login.acessarPortalEfetivdade(setor);
        login.acessarPortalGDU();


        if (!validarSeOWebElementEExibido(comunsElements.botaoOpcoes )
                && !validarSeOWebElementEExibido(comunsElements.linhaStepByStep)) {
            ReportCucumber.report(getBrowser(), "Usuario sem acesso ao portal GDU", true);
        } else {
            ReportCucumber.report(getBrowser(), "Usuario esta com acesso ao portal GDU. - FALHA\n" +
                    "Esperado que usuario logado nao tenha acesso ao portal.", true);
            Assert.fail("Usuario esta com acesso ao portal GDU. - FALHA\n" +
                    "Esperado que usuario logado nao tenha acesso ao portal.");
        }
    }

        public void validarAcesso_GDU(String setor) throws InterruptedException, IOException {
            login.realizar_Logoff();
            login.acessarPortalEfetivdade(setor);
            login.acessarPortalGDU();

            if (validarSeOWebElementEExibido(comunsElements.botaoOpcoes)
                    || validarSeOWebElementEExibido(comunsElements.linhaStepByStep)){
                ReportCucumber.report(getBrowser(), "Usuario com acesso ao portal GDU", true);
            }else {
                ReportCucumber.report(getBrowser(), "Usuario esta sem acesso ao portal GDU. - FALHA\n" +
                        "Esperado que tenha acesso ao portal GDU", true);
                Assert.fail("Usuario esta sem acesso ao portal GDU. - FALHA\n" +
                        "Esperado que usuario logado tenha acesso ao portal GDU.");
            }


//        RestAssured.authentication = basic("foo", "bar");
//        RestAssured.baseURI = "https://tools-efetividade-qas.gruponc.net.br/";
//        RestAssured.useRelaxedHTTPSValidation();
//
//        Map<String, String> login = new HashMap<String, String>();
//        login.put("txtSetor", "11200000");
//        login.put("txtSenha", "123");
//
//
//        String token = given()
//                .log().all()
//                .body(login)
//                .contentType(ContentType.JSON)
//        .when()
//                .post("https://tools-efetividade-qas.gruponc.net.br/logar")
//        .then()
//                .log().ifError()
//                .statusCode(302)
//                .extract().asString();
////                .extract().cookie("PHPSESSID");
//
//
//        System.out.println("AQUIII: "+token);
//

//        given()
//                .log().all()
//        .when()
//                .get("https://tools-efetividade-qas.gruponc.net.br/sistemas/GDU/api/auth/1")
//        .then()
//                .log().all()
//                .statusCode(200);
//
//        given()
//                .log().all()
//                .header("Authorization", "MOCKSESSID" + mocksessid)
//        .when()
//                .post("https://gdu-backend-qa.gruponc.net.br/api/user/auth")
//        .then()
//                .log().all()
//                .statusCode(200);

    }
}




