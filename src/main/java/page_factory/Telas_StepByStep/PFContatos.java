package page_factory.Telas_StepByStep;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page_factory.ComunsElements.PFComunsElements;

import static drivers.DriverInit.getBrowser;
import static methods.Methods.*;

public class PFContatos {

    PFComunsElements comunsElements = new PFComunsElements();

    public PFContatos(){
        PageFactory.initElements(getBrowser(), this);
    }

    @FindBy(xpath = "//span[.='addNovo telefone']")
    private WebElement botaoNovoTelefone;

    @FindBy(xpath = "//div[@id='phone-type-select']")
    private WebElement botaoSelectTipo;

    @FindBy(xpath = "//input[@class='MuiInputBase-input MuiOutlinedInput-input']")
    private WebElement editavelTelefone;

    @FindBy(css = ".MuiInputBase-multiline textarea:nth-child(1)")
    private WebElement editavelObservacacao;

    @FindBy(xpath = "//span[.='saveSalvar']")
    private WebElement botaoSalvar;

    @FindBy(css = "div:nth-of-type(1) > .MuiButtonBase-root > .MuiButtonBase-root span:nth-child(1)")
    private WebElement botaoPrimeiroTelefone;

    @FindBy(xpath = "//span[.='Telefones']")
    private WebElement abaTelefones;

    @FindBy(xpath = "//span[.='E-mails']")
    private WebElement abaEmail;

    @FindBy(xpath = "//span[.='addNovo e-mail']")
    private WebElement botaoNovoEmail;

    @FindBy(xpath = "//input[@class='MuiInputBase-input MuiOutlinedInput-input']")
    private WebElement editavelEmail;

    @FindBy(css = ".MuiInputBase-multiline textarea:nth-child(1)")
    private WebElement editavelObservacaoEmail;

    @FindBy(css = "div:nth-of-type(2) > .MuiButtonBase-root > .MuiButtonBase-root span svg")
    private WebElement botaoPrimeiroEmail;

    public void cadastrarNumeroNovo(String tipoTelefone, String numeroTelefone, String observacao){
        clicarElemento(botaoNovoTelefone);
        inserirTexto_Telefone(numeroTelefone);
        selecionarOpcao_TipoTelefone(tipoTelefone);
        inserirTexto_Observacao(observacao);
        clicarElemento(botaoSalvar);
    }

    public void cadastrarEmailNovo(String email, String observacao){
        clicarElemento(botaoNovoEmail);
        aguardar(1);
        inserirTexto_Email(email);
        inserirTexto_ObservacaoEmail(observacao);
        clicarElemento(botaoSalvar);
    }

    public void editarEmailCadastrado(String email, String observacao){
        clicarElemento(botaoPrimeiroEmail);
        inserirTexto_Email(email);
        inserirTexto_ObservacaoEmail(observacao);
        clicarElemento(botaoSalvar);
    }

    public void editarNumeroCadastrado(String tipoTelefone, String numeroTelefone, String observacao){
        clicarElemento(botaoPrimeiroTelefone);
        selecionarOpcao_TipoTelefone(tipoTelefone);
        inserirTexto_Telefone(numeroTelefone);
        inserirTexto_Observacao(observacao);
        clicarElemento(botaoSalvar);
    }

    public void acessarAba_Telefones(){
        clicarElemento(abaTelefones);
    }

    public void acessarAba_Email(){
        clicarElemento(abaEmail);
    }

    private void selecionarOpcao_TipoTelefone(String tipoTelefone){
        aguardar(2);
        clicarElemento(botaoSelectTipo);
        percorrerListaDeElementosEclicarNoTexto(comunsElements.listaSelectGDU, tipoTelefone);
    }

    private void inserirTexto_Telefone(String numeroTelefone){
        aguardar(2);
        inserirTexto_GDU(editavelTelefone, numeroTelefone);
    }

    private void inserirTexto_Observacao(String observacao){
        inserirTexto_GDU(editavelObservacacao, observacao);
    }

    private void inserirTexto_Email(String email){
        inserirTexto_GDU(editavelEmail, email);
    }

    private void inserirTexto_ObservacaoEmail(String observacao){
        inserirTexto_GDU(editavelObservacaoEmail, observacao);
    }
}
