package page_factory.Telas_StepByStep;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page_factory.ComunsElements.PFComunsElements;
import report.ReportCucumber;

import static drivers.DriverInit.getBrowser;
import static methods.Methods.*;

public class PFDadosPessoais {

    PFComunsElements comunsElements = new PFComunsElements();

    public PFDadosPessoais(){
        PageFactory.initElements(getBrowser(), this);
    }

    @FindBy(xpath = "//div[.='Nome completo']/input")
    private WebElement campoNomeCompleto;

    @FindBy(xpath = "//div[.='Matrícula']/input")
    private WebElement campoMatricula;

    @FindBy(xpath = "//div[.='CPF']/input")
    private WebElement campoCPF;

    @FindBy(xpath = "//div[.='ID de rede']/input")
    private WebElement campoIdDeRede;

    @FindBy(css = ".MuiInputBase-adornedStart > input")
    private WebElement editavelDataNascimento;

    @FindBy(xpath = "//div[.='Como você deseja ser chamado (a) ?']/input")
    private WebElement editavelApelido;

    @FindBy(xpath = "//div[.='RG']")
    private WebElement editavelRG;

    @FindBy(css = "div:nth-of-type(9) .MuiInputBase-root")
    private WebElement botaoEstadoCivil;

    @FindBy(css = "div:nth-of-type(10) .MuiInputBase-root")
    private WebElement botaoSexo;

    @FindBy(css = "div:nth-of-type(11) .MuiInputBase-root")
    private WebElement botaoNumeroCamisa;

    @FindBy(css = "div.formControlCheckboxes > label:nth-of-type(1)")
    private WebElement botaoFumante;

    @FindBy(css = "div.formControlCheckboxes > label:nth-of-type(2)")
    private WebElement botaoPCD;

    @FindBy(xpath = "//button[@class='MuiButtonBase-root MuiButton-root MuiButton-contained MuiButton-containedPrimary']")
    private WebElement botaoSalvarDados;

    @FindBy(xpath = "//button[.='Avançarnavigate_next']")
    private WebElement botaoAvancar;

    @FindBy(xpath = "//button[.='arrow_backVoltar']")
    private WebElement botaoVoltar;

    public void depararCamposNomeMatriculaCPFIdRede_EDesabilitados(String nome, String matricula, String cpf, String idRede){
        Assert.assertTrue(obterValueDoElemento(campoNomeCompleto).toLowerCase().equals(nome.toLowerCase())
                && obterAtributoDoElemento(campoNomeCompleto, "class").contains("disabled"));

        Assert.assertTrue(obterValueDoElemento(campoMatricula).toLowerCase().equals(matricula.toLowerCase())
                && obterAtributoDoElemento(campoMatricula, "class").contains("disabled"));

        Assert.assertTrue(obterValueDoElemento(campoCPF).toLowerCase().equals(cpf.toLowerCase())
                && obterAtributoDoElemento(campoCPF, "class").contains("disabled"));

        Assert.assertTrue(obterValueDoElemento(campoIdDeRede).toLowerCase().equals(idRede.toLowerCase())
                && obterAtributoDoElemento(campoIdDeRede, "class").contains("disabled"));
    }

    public void preencherCampos(String dataNasc, String apelido, String estadoCivil,String sexo, String numCamisa, boolean fumante, boolean pcd){
        preencherCampo_DataNascimento(dataNasc);
        preencherCampo_Apelido(apelido);
        selecionarOpcao_EstadoCivil(estadoCivil);
        selecionarOpcao_NumeroCamisa(numCamisa);
        selecionarOpcao_Sexo(sexo);
        checkarCampo_Fumante(fumante);
        checkarCampo_PCD(pcd);

        clicarElemento(botaoSalvarDados);
    }

    public void validarMensagemDeSucessoExibida(){
        if(validarSeOWebElementEExibido(comunsElements.spamMsgUsuario)
        && obterTexto(comunsElements.spamMsgUsuario).equals("Dados salvos com sucesso!")){
            ReportCucumber.report(getBrowser(), "Mensagem de sucesso exibida. Edição realizada com sucesso",true);
        }else{
            ReportCucumber.report(getBrowser(), "Mensagem de salvom com sucesso nao foi exibida. - FALHA", true);
            Assert.fail("Mensagem de salvom com sucesso nao foi exibida. - FALHA");
        }
    }

    private void preencherCampo_DataNascimento(String dataNasc){
        clicarElemento(editavelDataNascimento);
        apagarTexto_GDU(editavelDataNascimento);
        inserirTexto(editavelDataNascimento, dataNasc);
    }

    private void preencherCampo_Apelido(String apelido){
        clicarElemento(editavelApelido);
        apagarTexto_GDU(editavelApelido);
        inserirTexto(editavelApelido, apelido);
    }

    private void preencherCampo_RG(String rg){
        clicarElemento(editavelRG);
        apagarTexto_GDU(editavelRG);
        inserirTexto(editavelRG, rg);
    }

    private void selecionarOpcao_EstadoCivil(String estadoCivil){
        aguardar(1);
        clicarElemento(botaoEstadoCivil);
        percorrerListaDeElementosEclicarNoTexto(comunsElements.listaSelectGDU, estadoCivil);
    }

    private void selecionarOpcao_NumeroCamisa(String numeroCamisa){
        aguardar(2);
        clicarElemento(botaoNumeroCamisa);
        percorrerListaDeElementosEclicarNoTexto(comunsElements.listaSelectGDU, numeroCamisa);
    }

    private void selecionarOpcao_Sexo(String sexo){
        aguardar(2);
        clicarElemento(botaoSexo);
        percorrerListaDeElementosEclicarNoTexto(comunsElements.listaSelectGDU, sexo);
    }

    private void checkarCampo_Fumante(boolean fumante){
        if(obterTexto(botaoFumante).equals("Fumante (Não)")
        && fumante){
            clicarElemento(botaoFumante);
        }else if (obterTexto(botaoFumante).equals("Fumante (Sim)")
        && !fumante){
            clicarElemento(botaoFumante);
        }
    }

    private void checkarCampo_PCD(boolean pcd){
        if(obterTexto(botaoPCD).equals("PCD (Não)")
                && pcd){
            clicarElemento(botaoPCD);
        }else if (obterTexto(botaoPCD).equals("PCD (Sim)")
                && !pcd){
            clicarElemento(botaoPCD);
        }
    }
}
