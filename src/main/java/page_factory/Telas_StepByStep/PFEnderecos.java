package page_factory.Telas_StepByStep;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page_factory.ComunsElements.PFComunsElements;
import report.ReportCucumber;

import java.util.concurrent.locks.ReentrantReadWriteLock;

import static drivers.DriverInit.getBrowser;
import static methods.Methods.*;

public class PFEnderecos {

    PFComunsElements comunsElements = new PFComunsElements();

    public PFEnderecos(){
        PageFactory.initElements(getBrowser(), this);
    }

    @FindBy(xpath = "//input[@id='postal-code-input']")
    private WebElement editavelCEP;

    @FindBy(xpath = "//div[.='Logradouro']/input")
    private WebElement editavelLogradouro;

    @FindBy(xpath = "//div[.='Bairro']/input")
    private WebElement editavelBairro;

    @FindBy(xpath = "//div[.='Cidade']/input")
    private WebElement editavelCidade;

    @FindBy(xpath = "//div[@id='uf-select']")
    private WebElement botaoEstado;

    @FindBy(xpath = "//div[.='Número']/input")
    private WebElement editavelNumero;

    @FindBy(css = ".MuiInputBase-multiline textarea:nth-child(1)")
    private WebElement editavelComplemento;

    @FindBy(xpath = "(//div[.='ResidencialAmostra']/label)[1]/span[1]")
    private WebElement checkResidencial;

    @FindBy(xpath = "(//div[.='ResidencialAmostra']/label)[2]/span[1]")
    private WebElement checkAmostra;

    @FindBy(xpath = "//span[.='saveSalvar']")
    private WebElement botaoSalvar;

    @FindBy(xpath = "//div[@class='App']/div/div[3]/button[2]")
    private WebElement botaoAvancar;

    @FindBy(xpath = "//span[.='Meus Endereços']")
    private WebElement botaoMeusEnderecos;

    @FindBy(xpath = "//span[.='Cadastrar']")
    private WebElement botaoCadastrar;

    @FindBy(css = "div[aria-labelledby='simple-tab-1'] > div:nth-of-type(1)  > div:nth-of-type(1) svg")
    private WebElement botaoPrimeiroEnderecoCadastrado;

    @FindBy(css = "div[aria-labelledby='simple-tab-1'] > div:nth-of-type(1)  > div:nth-of-type(2) svg")
    private WebElement botaoSegundoEnderecoCadastrado;


    public void preencherCampos_Endereco(String cep, String logradouro, String bairro, String cidade, String uf,
                                         String numero, String complemento, boolean residencial, boolean amostra){
        inserirTexto_CEP(cep);
        inserirTexto_Logradouro(logradouro);
        inserirTexto_Bairro(bairro);
        inserirTexto_Cidade(cidade);
        selecionarOpcao_Estado(uf);
        inserirTexto_Numero(numero);
        inserirTexto_Complemento(complemento);
        checkarCampo_Residencial(residencial);
        checkarCampo_Amostra(amostra);

        clicarElemento(botaoSalvar);
    }

    public void acessarAba_MeusEnderecos(){
        clicarElemento(botaoMeusEnderecos);
        ReportCucumber.report("Aba: Meus enderecos aberto com sucesso.");
    }

    public void acessarAba_Cadastrar(){
        clicarElemento(botaoCadastrar);
        ReportCucumber.report("Aba: Cadastrar aberto com sucesso.");
    }

    public void clicarPrimeiroEnderecoCadastrado(){
        aguardar(1);
        clicarElemento(botaoPrimeiroEnderecoCadastrado);
        ReportCucumber.report("Primeiro endereco aberto com sucesso.");
    }

    public void validarEnderecoResidencialFoiSobrescrito(){
        verificarSeSegundoEnderoCadastradoEhResidencial();
        verificarSePrimeiroEnderoCadastradoNaoEhResidencial();
    }

    public void validarSeBotaoAvancarEstaHabilitado(){
        if(!obterAtributoDoElemento(botaoAvancar, "class").contains("Mui-disabled")){
            ReportCucumber.report(getBrowser(), "Botao avancar esta habilitado.", true);
        }else {
            ReportCucumber.report(getBrowser(),"Botao avancar esta desabilitado. - FALHA", true);
            Assert.fail("Botao avancar esta desabilitado. - FALHA");
        }
    }

    public void verificarSePrimeiroEnderecoCadastradoEhResidencial(){
        clicarElemento(botaoPrimeiroEnderecoCadastrado);
        if (obterAtributoDoElemento(checkResidencial, "class").contains("Mui-checked")){
            ReportCucumber.report(getBrowser(), "O primeiro endereco esta marcado como residencial.", true);
        }else {
            ReportCucumber.report(getBrowser(), "O primeiro endereco nao esta marcado como residencial. - FALHA", true);
            Assert.fail("O primeiro endereco nao esta marcado como residencial. - FALHA");
        }
    }

    public void verificarSePrimeiroEnderoCadastradoNaoEhResidencial(){
        clicarElemento(botaoPrimeiroEnderecoCadastrado);
        if (!obterAtributoDoElemento(checkResidencial, "class").contains("Mui-checked")){
            ReportCucumber.report(getBrowser(), "O primeiro endereco nao esta marcado como residencial. - FALHA", true);
        }else {
            ReportCucumber.report(getBrowser(), "O primeiro endereco nao esta marcado como residencial.", true);
            Assert.fail("O primeiro endereco esta marcado como residencial. - FALHA");
        }
    }

    public void verificarSeSegundoEnderoCadastradoEhResidencial(){
        clicarElemento(botaoSegundoEnderecoCadastrado);
        clicarElemento(botaoSalvar);
        if (obterAtributoDoElemento(checkResidencial, "class").contains("Mui-checked")){
            ReportCucumber.report(getBrowser(), "O primeiro endereco esta marcado como residencial. - FALHA", true);
        }else {
            ReportCucumber.report(getBrowser(), "O primeiro endereco nao esta marcado como residencial. - FALHA", true);
            Assert.fail("O primeiro endereco nao esta marcado como residencial. - FALHA");
        }
    }

    public void validarSeBotaoAvancarEstaDesabilitado(){
        if(obterAtributoDoElemento(botaoAvancar, "class").contains("Mui-disabled")){
            ReportCucumber.report(getBrowser(),"Botao avancar esta desabilitado com sucesso.", true);
        }else {
            ReportCucumber.report(getBrowser(), "Botao avancar esta habilitado. - FALHA", true);
            Assert.fail("Botao avancar esta habilitado. - FALHA");
        }
    }


    private void inserirTexto_CEP(String cep){
        aguardar(1);
        if (!cep.isEmpty())
        inserirTexto_GDU(editavelCEP, cep);
    }

    private void inserirTexto_Logradouro(String logradouro){
        aguardar(3);
        if (!logradouro.isEmpty())
        inserirTexto_GDU(editavelLogradouro, logradouro);
    }

    private void inserirTexto_Bairro(String bairro){
        aguardar(1);
        if (!bairro.isEmpty())
        inserirTexto_GDU(editavelBairro, bairro);
    }

    private void inserirTexto_Cidade(String cidade){
        aguardar(1);
        if (!cidade.isEmpty())
        inserirTexto_GDU(editavelCidade, cidade);
    }

    private void selecionarOpcao_Estado(String estado){
        aguardar(1);
        if (!estado.isEmpty())
        selecionarTextoSelect_GDU(botaoEstado, comunsElements.listaSelectGDU, estado);
    }

    private void inserirTexto_Numero(String numero){
        aguardar(1);
        if (!numero.isEmpty())
        inserirTexto_GDU(editavelNumero, numero);
    }

    private void inserirTexto_Complemento(String complemento){
        aguardar(1);
        if (!complemento.isEmpty())
        inserirTexto_GDU(editavelComplemento, complemento);
    }

    private void checkarCampo_Residencial(boolean residencial){

        if (!obterAtributoDoElemento(checkResidencial, "class").contains("Mui-checked")
        && residencial){
            clicarElemento(checkResidencial);
        }else if(obterAtributoDoElemento(checkResidencial, "class").contains("Mui-checked")
        && !residencial){
            clicarElemento(checkResidencial);
        }
    }

    private void checkarCampo_Amostra(boolean amostra){
        if (!obterAtributoDoElemento(checkAmostra, "class").contains("Mui-checked")
                && amostra){
            clicarElemento(checkAmostra);
        }else if(obterAtributoDoElemento(checkAmostra, "class").contains("Mui-checked")
                && !amostra){
            clicarElemento(checkAmostra);
        }
    }


}
