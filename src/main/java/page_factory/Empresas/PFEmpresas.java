package page_factory.Empresas;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page_factory.ComunsElements.PFComunsElements;
import report.ReportCucumber;

import java.util.ArrayList;
import java.util.List;

import static drivers.DriverInit.getBrowser;
import static methods.Methods.*;

public class PFEmpresas {

    PFComunsElements comunsElements = new PFComunsElements();

    public PFEmpresas(){
        PageFactory.initElements(getBrowser(), this);
    }

    @FindBy(xpath = "//button[.='add_box']")
    private WebElement botaoAddNovaEmpresa;

    @FindBy(css = "input[type='number']")
    private WebElement editavelCodigo;

    @FindBy(css = "[placeholder='Nome']")
    private WebElement editavelNome;

    @FindBy(css = ".MuiTableBody-root.MuiTableBody-root td:nth-child(4) > span")
    private WebElement botaoAtivarEmpresa;

    @FindBy(xpath = "(//span[.='check'])[2]")
    private WebElement botaoSalvar;

    @FindBy(css = "[placeholder='Pesquisar empresa']")
    private WebElement editavelPesquisar;

    @FindBy(css = "button[title='Editar empresa'] .material-icons")
    private WebElement botaoEditarEmpresa;

    @FindBy(css = ".MuiTableBody-root td:nth-child(2)")
    private WebElement campoParaValidarInativa;

    @FindBy(xpath = "//div[.='Ativa']")
    private WebElement botaoAtiva;

    @FindBy(css = ".MuiTableBody-root > tr > td:nth-child(3)")
    private List<WebElement> listInativas;

    @FindBy(xpath = "//span[.='businessEmpresas']")
    private WebElement botaoTelaEmpresas;


    public void criarEmpresa(String codigoEmpresa, String nomeEmpresa, boolean empresaAtiva){
        clicarElemento(botaoAddNovaEmpresa);
        inserirTexto_GDU(editavelCodigo, codigoEmpresa);
        inserirTexto_GDU(editavelNome, nomeEmpresa);

        ativarDesativarEmpresa(empresaAtiva);
        clicarElemento(botaoSalvar);
    }

    public void editarEmpresa(String codigoEmpresa, String nomeEmpresa, boolean empresaAtiva){
        aguardar(1);
        clicarElemento(botaoEditarEmpresa);
        inserirTexto_GDU(editavelCodigo, codigoEmpresa);
        inserirTexto_GDU(editavelNome, nomeEmpresa);

        ativarDesativarEmpresa(empresaAtiva);
        clicarElemento(botaoSalvar);
    }

    public void pesquisarEmpresa(String pesquisarEmpresa){
        inserirTexto_GDU(editavelPesquisar, pesquisarEmpresa);
        ReportCucumber.report("Empresa pesquisada com sucesso.");
    }

    public void validarEmpresaInativa(){
        if (obterValorCSSDoElemento(campoParaValidarInativa, "color").equals("rgba(255, 0, 0, 1)")){
            ReportCucumber.report(getBrowser(), "Empresa Inativa foi validada com sucesso.", true);
        }else {
            ReportCucumber.report(getBrowser(), "Empresa esta ativa, validacao nao realizada com sucesso. - FALHA", true);
            Assert.fail("Empresa esta ativa, validacao nao realizada com sucesso. - FALHA");
        }
    }

    public void validarEmpresaAtiva(){
        if (obterValorCSSDoElemento(campoParaValidarInativa, "color").equals("rgba(0, 0, 0, 1)")){
            ReportCucumber.report(getBrowser(), "Empresa Ativa foi validada com sucesso.", true);
        }else {
            ReportCucumber.report(getBrowser(), "Empresa esta inativa, validacao nao realizada com sucesso. - FALHA", true);
            Assert.fail("Empresa esta inativa, validacao nao realizada com sucesso. - FALHA");
        }
    }

    public List<String> obterEmpresasInativas(){
        acessarTela_Empresa();
        aguardar(5);
        clicarElemento(botaoAtiva);

       List<String> empresasInativas = new ArrayList<String>();

        for (WebElement element : listInativas) {
            if (obterValorCSSDoElemento(element, "color").equals("rgba(255, 0, 0, 1)")) {
                empresasInativas.add(obterTexto(element));
            } else if (obterValorCSSDoElemento(element, "color").equals("rgba(0, 0, 0, 1)")) {
                break;
            }
        }
        return empresasInativas;
    }

    private void ativarDesativarEmpresa(boolean empresaAtiva){
        if (!obterAtributoDoElemento(botaoAtivarEmpresa, "class").contains("Mui-checked")
                && empresaAtiva){
            clicarElemento(botaoAtivarEmpresa);
            ReportCucumber.report("Empresa ativada com sucesso.");
        }

      if (obterAtributoDoElemento(botaoAtivarEmpresa, "class").contains("Mui-checked")
                && !empresaAtiva){
            clicarElemento(botaoAtivarEmpresa);
            ReportCucumber.report("Empresa desativada com sucesso");
        }
    }

    public void acessarTela_Empresa(){
        clicarElemento(botaoTelaEmpresas);
        ReportCucumber.report("Tela Empresas acessada com sucesso.");
    }
}
