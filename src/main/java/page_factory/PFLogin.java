package page_factory;

import drivers.DriverInit;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import page_factory.ComunsElements.PFComunsElements;
import report.ReportCucumber;
import static drivers.DriverInit.getBrowser;
import static methods.Methods.*;

public class PFLogin {

    DriverInit driverInit = new DriverInit();
    PFComunsElements comunsElements = new PFComunsElements();

    public PFLogin(){
        PageFactory.initElements(getBrowser(), this);
    }

    @FindBy(name = "txtSetor")
    private WebElement editavelSetor;

    @FindBy(name = "txtSenha")
    private WebElement editavelSenha;

    @FindBy(css = ".btn")
    private WebElement botaoAcessar;

    @FindBy(css = "div#panel-colab .pull-left")
    private WebElement textoBoasVindas_Efetividade;

    @FindBy(id = "btnAbrir")
    private WebElement botaoAbrirMenu;

    @FindBy(xpath = "//label[.='Gestão de Usuários']")
    private WebElement botaoPortalGDU;

    @FindBy(xpath = "//span[.='Opções']")
    private WebElement botaoOpcoes;

    @FindBy(css = "a[data-target='#modalLogout'] > .glyphicon")
    private WebElement botaoLogoff;

    @FindBy(css = "button.btn-info")
    private WebElement botaoConfirmarLogoff;

    @FindBy(xpath = "//span[.='person_addUnidades de negócio']")
    private WebElement botaoUnidadesDeNegocio;

    @FindBy(xpath = "//span[.='blockBloquear colaborador']")
    private WebElement botaoBloquearColaborador;

    @FindBy(xpath = "//span[.='swap_vertPromover colaborador']")
    private WebElement botaoPromoverColaborador;

    @FindBy(xpath = "//span[.='contact_phoneContato']")
    private WebElement botaoContato;

    @FindBy(xpath = "//span[.='homeEndereço']")
    private WebElement botaoEndereco;

    @FindBy(xpath = "//span[.='businessEmpresas']")
    private WebElement botaoEmpresas;

    @FindBy(xpath = "//span[.='person_addAlocar colaborador']")
    private WebElement botaoAlocarColaborador;

    @FindBy(xpath = "//span[.='transfer_within_a_stationTransferir colaborador']")
    private WebElement botaoTransferirColaborador;

    public void realizar_Logoff(){
        sair_iFrame();
        clicarElemento(botaoLogoff);
        clicarElemento(botaoConfirmarLogoff);
        ReportCucumber.report("Logoff realizado com sucesso");
    }

    public void acessarPortalEfetivdade(String usuario){
        driverInit.acessarPagina("https://tools-efetividade-qas.gruponc.net.br/login/");

        inserirTexto(editavelSetor, usuario);
        inserirTexto(editavelSenha, "a");
        clicarElemento(botaoAcessar);
        validarAcessoAoPortalEfetividade();
    }

    public void acessarPortalGDU() throws InterruptedException {
        clicarElemento(botaoAbrirMenu);
        aguardar(1);
        scrollToElementJS(botaoPortalGDU);
        clicarElemento(botaoPortalGDU);
        validarAcessoAoPortalGDU();
        comunsElements.acessarIframesGDU();
    }

    public void acessarTela_GDU(String nomeTela){
        acessarTelas(nomeTela);
    }

    private void validarAcessoAoPortalEfetividade(){
        if (validarSeOWebElementEExibido(textoBoasVindas_Efetividade)){
            ReportCucumber.report(getBrowser(),"Mensagem de boas vindas encontrado com sucesso, portal efetividade" +
                    " acessado com sucesso.", true);
        }else {
            ReportCucumber.report(getBrowser(), "Mensagem de boas vindas não encontrado. - FALHA ", true);
            Assert.fail("Mensagem de boas vindas não encontrado. - FALHA");
        }
    }

    private void validarAcessoAoPortalGDU() throws InterruptedException {
        aguardar(5);
        if (obterUrlAtual().contains("https://tools-efetividade-qas.gruponc.net.br/ferramenta/abrir/25/Gest%C3%A3o%20de%20Usu%C3%A1rios")){
            ReportCucumber.report(getBrowser(),"Portal GDU acessado com sucesso.", true);
        }else {
            ReportCucumber.report(getBrowser(), "Portal GDU não foi acessado com sucesso. - FALHA", true);
            Assert.fail("Portal GDU não foi acessado com sucesso. - FALHA");
        }
    }

    private void acessarTelas(String nomeTela){
        switch (nomeTela){
            case "Unidades de negócio":
                clicarElemento(botaoUnidadesDeNegocio);
                break;

            case "Bloquear colaborador":
                clicarElemento(botaoBloquearColaborador);
                break;

            case "Promover colaborador":
                clicarElemento(botaoPromoverColaborador);
                break;

            case "Contato":
                clicarElemento(botaoContato);
                break;

            case "Endereços":
                clicarElemento(botaoEndereco);
                break;

            case "Empresas":
                clicarElemento(botaoEmpresas);
                break;

            case "Alocar colaborador":
                clicarElemento(botaoAlocarColaborador);
                break;

            case "Transferir colaborador":
                clicarElemento(botaoTransferirColaborador);
                break;

            default:
                ReportCucumber.report("Tela inserida nao existe. - FALHA");
                Assert.fail("Tela inserida nao existe. - FALHA");
                break;
        }

    }
}
