package methods;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import report.ReportCucumber;

import java.sql.*;
import java.util.List;

import static drivers.DriverInit.getBrowser;

public class Methods {

    public static void scrollToElement(@NotNull WebElement element){
        Actions actions = new Actions(getBrowser());
        actions.moveToElement(element);
        actions.perform();
    }

    public static void scrollToElementJS(@NotNull WebElement element){
        try{
            ((JavascriptExecutor) getBrowser()).executeScript("arguments[0].scrollIntoView(true);", element);
            ReportCucumber.report("Scroll realizado com sucesso.");
        }catch (Exception e){
            ReportCucumber.report(getBrowser(),"Nao foi possivel executar o scroll. \n Erro: " + e.getMessage(), true);
            Assert.fail("Nao foi possivel executar o scroll. \n Erro: " + e.getMessage());
        }
    }
    public static void inserirTexto(@NotNull WebElement element, @NotNull String texto){
        try{
            element.sendKeys(texto);
            ReportCucumber.report("Texto inserido com sucesso.");
        }catch (Exception e){
            ReportCucumber.report(getBrowser(),"Nao foi possivel inserir o texto no elemento desejado. \n Erro: " + e.getMessage(), true);
            Assert.fail("Nao foi possivel inserir o texto no elemento desejado. \n Erro: " + e.getMessage());
        }
    }

    public static String obterValorCSSDoElemento(@NotNull WebElement element, @NotNull String value){
        String valor = null;
        try {
            valor = element.getCssValue(value);
        }catch (Exception e){
            ReportCucumber.report(getBrowser(),"Nao foi possivel obter o valor desejado. \n Erro: " + e.getMessage(), true);
            Assert.fail("Nao foi possivel obter o valor desejado. \n Erro: " + e.getMessage());
        }
        return valor;
    }

    public static void inserirTexto_GDU(@NotNull WebElement element, @NotNull String texto){
        try{
            if (!texto.equals("")){
                clicarElemento(element);
                apagarTexto_GDU(element);
                inserirTexto(element, texto);
                ReportCucumber.report("Texto inserido com sucesso.");
            }
        }catch (Exception e){
            ReportCucumber.report(getBrowser(),"Nao foi possivel inserir o texto no elemento desejado. \n Erro: " + e.getMessage(), true);
            Assert.fail("Nao foi possivel inserir o texto no elemento desejado. \n Erro: " + e.getMessage());
        }
    }

    public static void selecionarTextoSelect_GDU(@NotNull WebElement element, @NotNull List<WebElement> listWebElement, @NotNull String texto){
        try {
            clicarElemento(element);
            aguardar(3);
            percorrerListaDeElementosEclicarNoTexto(listWebElement, texto);
            ReportCucumber.report("Elemento selecionado com sucesso.");
        }catch (Exception e){
            ReportCucumber.report(getBrowser(), "Nao foi possivel selecionar o elemento no select desejado. - FALHA", true);
            Assert.fail("Nao foi possivel selecionar o elemento no select desejado. - FALHA");
        }
    }



    public static void limparTexto(@NotNull WebElement element){
        try{
            element.clear();
            ReportCucumber.report("Campo limpo com sucesso");
        }catch (Exception e){
            ReportCucumber.report(getBrowser(),"Nao foi possivel limpar o elemento desejado. \n Erro: " + e.getMessage(), true);
            Assert.fail("Nao foi possivel limpar o elemento desejado. \n Erro: " + e.getMessage());
        }
    }

    public static void clicarElemento(@NotNull WebElement elemento)
    {
        try
        {
            elemento.click();
            ReportCucumber.report("Elemento foi clicado com sucesso");
        }catch (Exception e)
        {
            ReportCucumber.report(getBrowser(),"Nao foi possivel clicar no elemento desejado. \n Erro: " + e.getMessage(), true);
            Assert.fail("Nao foi possivel clicar no elemento desejado. \n Erro: " + e.getMessage());
        }
    }

    public static String obterTexto(@NotNull WebElement elemento) {
        String texto = null;
        try {
            texto = elemento.getText();
            ReportCucumber.report("Texto obtido com sucesso.");
        } catch (Exception e) {
            ReportCucumber.report(getBrowser(),"Nao foi possivel obter texto do elemento desejado. \n Erro: " + e.getMessage(), true);
            Assert.fail("Nao foi possivel obter texto do elemento desejado. \n Erro: " + e.getMessage());
        }

        return texto;
    }

    public static String obterValueDoElemento(@NotNull WebElement elemento){
        String value = null;
        try {
            value = elemento.getAttribute("value");
            ReportCucumber.report("Value obtido com sucesso.");
        } catch (Exception e) {
            ReportCucumber.report(getBrowser(),"Nao foi possivel obter o value do elemento desejado. \n Erro: " + e.getMessage(), true);
            Assert.fail("Nao foi possivel obter o value do elemento desejado. \n Erro: " + e.getMessage());
        }

        return value;
    }

    public static String obterAtributoDoElemento(@NotNull WebElement elemento,@NotNull String atributo){
        String valor = null;
        try {
            valor = elemento.getAttribute(atributo);
            ReportCucumber.report("O atributo: "+atributo+" foi obtido com sucesso.");
        } catch (Exception e) {
            ReportCucumber.report(getBrowser(),"Nao foi possivel obter o atributo do elemento desejado. \n Erro: " + e.getMessage(), true);
            Assert.fail("Nao foi possivel obter o atributo do elemento desejado. \n Erro: " + e.getMessage());
        }

        return valor;
    }

    public static boolean validarSeOWebElementEExibido(@NotNull WebElement elemento){
        try {
            elemento.isDisplayed();
            ReportCucumber.report("O elemento procurado existe.");
            return true;
        }catch (NoSuchElementException e){
            ReportCucumber.report("O elemento procurado nao existe.");
            return false;
        }
    }

    public static void acessar_iFrame(@NotNull WebElement elemento){
        try {
            getBrowser().switchTo().frame(elemento);
            ReportCucumber.report("iFrame acessado com sucesso");
        }catch (Exception e){
            ReportCucumber.report(getBrowser(),"Nao foi possivel acessar o iFrame desjado \n Erro: " + e.getMessage(), true);
            Assert.fail("Nao foi possivel acessar o iFrame desejado \n Erro: " + e.getMessage());
        }
    }

    public static void sair_iFrame(){
        try {
            getBrowser().switchTo().defaultContent();
            ReportCucumber.report("Frame padrao acessado com sucesso.");
        }catch (Exception e){
            ReportCucumber.report(getBrowser(),"Não foi possivel retornar ao primeiro nivel da pagina. \n Erro: " + e.getMessage(), true);
            Assert.fail("Nao foi possivel retornar ao primeiro nivel da pagina. \n Erro: " + e.getMessage());
        }
    }

    public static void percorrerListaDeElementosEclicarNoTexto(@NotNull List<WebElement> listaDeElementos, @NotNull String texto){
        try{
            for (WebElement elemento : listaDeElementos){
                if (elemento.getText().contains(texto)){
                    clicarElemento(elemento);
                    ReportCucumber.report("Texto: <"+texto+"> encontrado e clicado com sucesso.");
                    break;
                }
            }
        }catch (Exception e){
            ReportCucumber.report(getBrowser(),"Nao foi possivel clicar no texto: <"+texto+"> desejado \n Erro: " + e.getMessage(), true);
            Assert.fail("Nao foi possivel clicar no texto: <"+texto+"> desejadoo \n Erro: " + e.getMessage());
        }
    }

    public static void selecionarSelectPorTexto(@NotNull WebElement elemento, @NotNull String textoVisivel){
        try {
            Select select = new Select(elemento);
            select.selectByVisibleText(textoVisivel);
            ReportCucumber.report("Select com o texto: <"+textoVisivel+"> selecionado com sucesso");
        }catch (Exception e){
            ReportCucumber.report(getBrowser(),"Nao foi possivel selecionar o texto: <"+textoVisivel+"> desejado \n Erro: " + e.getMessage(), true);
            Assert.fail("Nao foi possivel selecionar o texto: <"+textoVisivel+"> desejadoo \n Erro: " + e.getMessage());
        }
    }

    public static void selecionarSelectPorIndex(@NotNull WebElement elemento, @NotNull int index){
        try {
            Select select = new Select(elemento);
            select.selectByIndex(index);
            ReportCucumber.report("Index: <"+index+"> selecionado com sucesso");
        }catch (Exception e){
            ReportCucumber.report(getBrowser(),"Nao foi possivel selecionar o index: <"+index+"> desejado \n Erro: " + e.getMessage(), true);
            Assert.fail("Nao foi possivel selecionar o index: <"+index+"> desejadoo \n Erro: " + e.getMessage());
        }
    }

    public static String obterUrlAtual(){
        String url = null;
        try {
            url = getBrowser().getCurrentUrl();
            ReportCucumber.report("Url obtida com sucesso.");
        } catch (Exception e) {
            ReportCucumber.report(getBrowser(),"Nao foi possivel obter a url autual. \n Erro: " + e.getMessage(), true);
            Assert.fail("Nao foi possivel obter a url atual. \n Erro: " + e.getMessage());
        }

        return url;
    }

    public static void aguardar(int segundos){
        int aguardar = segundos * 1000;
        try {
            Thread.sleep(aguardar);
        }catch (Exception e){
            ReportCucumber.report(getBrowser(),"Nao foi possivel inserir o tempo de espera. \n Erro: " + e.getMessage(), true);
            Assert.fail("Nao foi possivel inserir o tempo de espera. \n Erro: " + e.getMessage());
        }
    }

    public static void acessarBancoMySQL() throws ClassNotFoundException, SQLException {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        Class.forName("com.mysql.jdbc.Driver");
        connection = DriverManager.getConnection("jdbc:mysql://localhost:3306", "root", "admin");

        statement = connection.createStatement();
    }

    public static boolean validarSeCampoEstaAtivado(@NotNull WebElement elemento){
        try {
            elemento.isEnabled();
            ReportCucumber.report("O elemento esta ativado.");
            return true;
        }catch (Exception e){
            ReportCucumber.report("O elemeno esta desabilitado.");
            return false;
        }
    }

    public static void apagarTexto_GDU(@NotNull WebElement elemento){
        elemento.sendKeys(Keys.END);
        for (int i = 0; i <=50 ; i++) {
            elemento.sendKeys(Keys.BACK_SPACE);
        }
    }
}