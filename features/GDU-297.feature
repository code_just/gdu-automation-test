#language: pt
Funcionalidade: GDU-297: Cadastrar empresas
  Eu como tester devo validar as regras de negocio e criterios de aceite dessa tela.

  @All @GDU-297 @297-01
  Esquema do Cenario: Criar empresa com sucesso
    Dado a abertura do "<navegador>"
    Quando insiro o usuario: "<usuario>"
    E acesso o portal GDU
    E acesso a tela: "Empresas"
    E edito os campos da tela empresa: "<codigoEmpresa>", "<nomeEmpresa>", "<empresaAtivada>"
    Entao valido se a mensagem: "Empresa cadastrada com sucesso" foi exibida com sucesso
  Exemplos:
    | navegador | usuario   | codigoEmpresa | nomeEmpresa   | empresaAtivada  |
    | chrome    | ems52346  | 2002          | Empresa Autm  | true            |

  @All @GDU-297 @297-02
  Esquema do Cenario: Editar empresa com sucesso
    Dado a abertura do "<navegador>"
    Quando insiro o usuario: "<usuario>"
    E acesso o portal GDU
    E acesso a tela: "Empresas"
    E pesquiso pela empresa: "<pesquisarEmpresa>"
    E edito os campos da tela empresa: "<codigoEmpresa>", "<nomeEmpresa>", "<empresaAtivada>", para edicao
    Entao valido se a mensagem: "Empresa editada com sucesso" foi exibida com sucesso
    Exemplos:
      | navegador | usuario   | codigoEmpresa | nomeEmpresa        | empresaAtivada  | pesquisarEmpresa   |
      | chrome    | ems52346  | 52            | Nome Editado Aut   | true            | 88                 |
      | chrome    | ems52346  | 88            | Empresa Edit       | true            | Nome Editado Aut   |

  @All @GDU-297 @297-03
  Esquema do Cenario: Inativar empresa com sucesso
    Dado a abertura do "<navegador>"
    Quando insiro o usuario: "<usuario>"
    E acesso o portal GDU
    E acesso a tela: "Empresas"
    E pesquiso pela empresa: "<pesquisarEmpresa>"
    E edito os campos da tela empresa: "<codigoEmpresa>", "<nomeEmpresa>", "<empresaAtivada>", para edicao
    E valido se a mensagem: "Empresa editada com sucesso" foi exibida com sucesso
    Entao valido se a empresa foi inativada com sucesso
    Exemplos:
      | navegador | usuario   | codigoEmpresa | nomeEmpresa        | empresaAtivada  | pesquisarEmpresa   |
      | chrome    | ems52346  |               |                    | false           | 1000               |

  @All @GDU-297 @297-04
  Esquema do Cenario: Ativar empresa com sucesso
    Dado a abertura do "<navegador>"
    Quando insiro o usuario: "<usuario>"
    E acesso o portal GDU
    E acesso a tela: "Empresas"
    E pesquiso pela empresa: "<pesquisarEmpresa>"
    E edito os campos da tela empresa: "<codigoEmpresa>", "<nomeEmpresa>", "<empresaAtivada>", para edicao
    E valido se a mensagem: "Empresa editada com sucesso" foi exibida com sucesso
    Entao valido se a empresa foi ativada com sucesso
    Exemplos:
      | navegador | usuario   | codigoEmpresa | nomeEmpresa        | empresaAtivada  | pesquisarEmpresa   |
      | chrome    | ems52346  |               |                    | true            | 1000               |

  @All @GDU-297 @297-05
  Esquema do Cenario: Validar que o sistema exiba o Toast de erro ao tentar cadastrar uma empresa com o mesmo nome
    Dado a abertura do "<navegador>"
    Quando insiro o usuario: "<usuario>"
    E acesso o portal GDU
    E acesso a tela: "Empresas"
    E edito os campos da tela empresa: "<codigoEmpresa>", "<nomeEmpresa>", "<empresaAtivada>"
    Entao valido se a mensagem: "Este valor já está sendo usado." foi exibida com sucesso
    Exemplos:
      | navegador | usuario   | codigoEmpresa | nomeEmpresa        | empresaAtivada  |
      | chrome    | ems52346  | 3666          | Mesmo Nome Aut     | true            |

  @All @GDU-297 @297-06
  Esquema do Cenario: Validar que o sistema exiba o Toast de erro ao tentar cadastrar uma empresa com o mesmo codigo
    Dado a abertura do "<navegador>"
    Quando insiro o usuario: "<usuario>"
    E acesso o portal GDU
    E acesso a tela: "Empresas"
    E edito os campos da tela empresa: "<codigoEmpresa>", "<nomeEmpresa>", "<empresaAtivada>"
    Entao valido se a mensagem: "Este valor já está sendo usado." foi exibida com sucesso
    Exemplos:
      | navegador | usuario   | codigoEmpresa | nomeEmpresa        | empresaAtivada  |
      | chrome    | ems52346  | 366           | Mesmo Cod Aut      | true            |