#language: pt
Funcionalidade: GDU-113: Cadastro de dados de contato
  Eu como tester devo validar as regras de negocio e criterios de aceite dessa tela.

  @All @GDU-113 @113-01
  Esquema do Cenario: Criar contato de telefone com sucesso
    Dado a abertura do "<navegador>"
    Quando insiro o usuario: "<usuario>"
    E acesso o portal GDU
    E acesso a tela: "Contato"
    E acesso a aba: Telefone
    E insiro nos campos de telefone com as informacoes: "<tipoTelefone>", "<numeroTelefone>", "<observacaoTelefone>", para criacao
    Entao valido se a mensagem: "Telefone registrado com sucesso!" foi exibida com sucesso
    Exemplos:
      | navegador | usuario   | tipoTelefone          | numeroTelefone  | observacaoTelefone  |
      | chrome    | 11200000  | Comercial             | 11 958300001    | Teste inclusao 01   |
      | chrome    | 11200000  | Residencial           | 11 958300002    | Teste inclusao 02   |
      | chrome    | 11200000  | Celular corporativo   | 11 958300003    | Teste inclusao 03   |
      | chrome    | 11200000  | Celular pessoal       | 11 958300004    | Teste inclusao 04   |

  @All @GDU-113 @113-02
  Esquema do Cenario: Editar contato com sucesso
    Dado a abertura do "<navegador>"
    Quando insiro o usuario: "<usuario>"
    E acesso o portal GDU
    E acesso a tela: "Contato"
    E acesso a aba: Telefone
    E insiro nos campos de telefone com as informacoes: "<tipoTelefone>", "<numeroTelefone>", "<observacaoTelefone>", para edicao
    Entao valido se a mensagem: "Telefone atualizado" foi exibida com sucesso
    Exemplos:
      | navegador | usuario   | tipoTelefone          | numeroTelefone  | observacaoTelefone  |
      | chrome    | 11200000  | Celular pessoal       | 11 977758324    | Teste edicao        |

  @All @GDU-113 @113-03
  Esquema do Cenario: Criar contato de email com sucesso
    Dado a abertura do "<navegador>"
    Quando insiro o usuario: "<usuario>"
    E acesso o portal GDU
    E acesso a tela: "Contato"
    E acesso a aba: Email
    E insiro nos campos de email com as informacoes: "<email>", "<observacao>", para criacao
    Entao valido se a mensagem: "E-mail registrado com sucesso!" foi exibida com sucesso
    Exemplos:
      | navegador | usuario   | email           | observacao    |
      | chrome    | 11200000  | teste@teste.com | Teste Criacao |

  @All @GDU-113 @113-04
  Esquema do Cenario: Criar contato de email com sucesso
    Dado a abertura do "<navegador>"
    Quando insiro o usuario: "<usuario>"
    E acesso o portal GDU
    E acesso a tela: "Contato"
    E acesso a aba: Email
    E insiro nos campos de email com as informacoes: "<email>", "<observacao>", para edicao
    Entao valido se a mensagem: "E-mail atualizado" foi exibida com sucesso
    Exemplos:
      | navegador | usuario   | email                  | observacao    |
      | chrome    | 11200000  | teste.edicao@teste.com | Teste Edicao  |




