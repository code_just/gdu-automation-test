#language: pt
Funcionalidade: GDU-323: Promover colaborador
  Eu como tester devo validar as regras de negocio e criterios de aceite dessa tela.

  @All @GDU-323 @323-01
  Esquema do Cenario: Validar que seja exibido somente as linhas liderada pelo usuario
    Dado a abertura do "<navegador>"
    Quando insiro o usuario: "<usuario>"
    E acesso o portal GDU
    E acesso a tela: "Promover colaborador"
    Entao valido que o usuario: "<usuario>" so pode ter acesso as linhas: "<linhas>"
    Exemplos:
      | navegador | usuario   | linhas                                                                                                                                   |
      | chrome    | 11300000  | Saude, Clinica                                                                                                                           |
      | chrome    | EMS52346  | Licitações, Sirius, Vital, Sinapse, Fenix, Clinica, Saude, Hospitalar, Underskin, Medical Doctor, Esmeralda, Focus, Atacado, Vendas, PDV |

  @All @GDU-323 @323-02
  Esquema do Cenario: Validar promoção realizada com sucesso
    Dado a abertura do "<navegador>"
    Quando insiro o usuario: "<usuario>"
    E acesso o portal GDU
    E acesso a tela: "Promover colaborador"
    E insiro as informacoes nos campos: "<linhaAtual>", "<setorAtual>" para: "<linhaNova>" e "<setorNovo>"
    Entao valido se a mensagem: "Promoção adicionado com sucesso!" foi exibida com sucesso
    Exemplos:
      | navegador | usuario   | linhaAtual | setorAtual | linhaNova | setorNovo |
      | chrome    | 11200000  | Clinica    | 11230300   | Clinica   | 11240000  |










#id_ems: 30785


