#language: pt
Funcionalidade: GDU-302: Cadastrar BUs
  Eu como tester devo validar as regras de negocio e criterios de aceite dessa tela.

  @All @GDU-302 @302-01
  Esquema do Cenario: Criar Unidade de negocio com sucesso
    Dado a abertura do "<navegador>"
    Quando insiro o usuario: "<usuario>"
    E acesso o portal GDU
    E acesso a tela: "Unidades de negócio"
    E edito os campos da tela de BU: "<codigoBU>", "<nomeBU>", "<nomeEmpresa>", "<tipoBU>", "<buAtivo>"
    Entao valido se a mensagem: "Unidade cadastrado com sucesso" foi exibida com sucesso
    Exemplos:
      | navegador | usuario   | codigoBU | nomeBU        | nomeEmpresa   | tipoBU  | buAtivo |
      | chrome    | ems52346  | 63       | BU Autmacao 2 | Empresa Autm  | Demanda | true    |

  @All @GDU-302 @302-02
  Esquema do Cenario: Editar Unidade de negocio com sucesso
    Dado a abertura do "<navegador>"
    Quando insiro o usuario: "<usuario>"
    E acesso o portal GDU
    E acesso a tela: "Unidades de negócio"
    E pesquiso pelo BU: "<pesquisarBU>"
    E edito os campos da tela de BU: "<nomeBU>", "<nomeEmpresa>", "<tipoBU>", "<buAtivo>", para edicao
    Entao valido se a mensagem: "Unidade atualizado com sucesso" foi exibida com sucesso
    Exemplos:
      | navegador | usuario   | nomeBU       | nomeEmpresa   | tipoBU    | buAtivo | pesquisarBU   |
      | chrome    | ems52346  | Teste        | EMS           | Comercial | true    | Teste Edicao  |
      | chrome    | ems52346  | Teste Edicao | Empresa Autm  | Demanda   | true    | Teste         |

  @All @GDU-302 @302-03
  Esquema do Cenario: Inativar Unidade de negocio com sucesso
    Dado a abertura do "<navegador>"
    Quando insiro o usuario: "<usuario>"
    E acesso o portal GDU
    E acesso a tela: "Unidades de negócio"
    E pesquiso pelo BU: "<pesquisarBU>"
    E edito os campos da tela de BU: "<nomeBU>", "<nomeEmpresa>", "<tipoBU>", "<buAtivo>", para edicao
    E valido se a mensagem: "Unidade atualizado com sucesso" foi exibida com sucesso
    Entao valido se o BU foi inativado com sucesso
    Exemplos:
      | navegador | usuario   | nomeBU       | nomeEmpresa   | tipoBU    | buAtivo | pesquisarBU   |
      | chrome    | ems52346  |              |               |           | false   | Teste         |

  @All @GDU-302 @302-04
  Esquema do Cenario: Ativar Unidade de negocio com sucesso
    Dado a abertura do "<navegador>"
    Quando insiro o usuario: "<usuario>"
    E acesso o portal GDU
    E acesso a tela: "Unidades de negócio"
    E pesquiso pelo BU: "<pesquisarBU>"
    E edito os campos da tela de BU: "<nomeBU>", "<nomeEmpresa>", "<tipoBU>", "<buAtivo>", para edicao
    E valido se a mensagem: "Unidade atualizado com sucesso" foi exibida com sucesso
    Entao valido se o BU foi ativado com sucesso
    Exemplos:
      | navegador | usuario   | nomeBU       | nomeEmpresa   | tipoBU    | buAtivo | pesquisarBU   |
      | chrome    | ems52346  |              |               |           | true    | Teste         |

#  @All @GDU-302 @302-05
#  Esquema do Cenario: Validar que seja possivel cadastrar um BU com o mesmo nome de um BU ja cadastrado
#    Dado a abertura do "<navegador>"
#    Quando insiro o usuario: "<usuario>"
#    E acesso o portal GDU
#    E acesso a tela: "Unidades de negócio"
#    E edito os campos da tela de BU: "<codigoBU>", "<nomeBU>", "<nomeEmpresa>", "<tipoBU>", "<buAtivo>"
#    Entao valido se a mensagem: "Unidade cadastrado com sucesso" foi exibida com sucesso
#    Exemplos:
#      | navegador | usuario   | codigoBU | nomeBU       | nomeEmpresa   | tipoBU  | buAtivo |
#      | chrome    | ems52346  | 99       | sfsd         | Empresa Autm  | Demanda | true    |

  @All @GDU-302 @302-06
  Esquema do Cenario: Validar que as empresas inativadas nao estao sendo exibidas campo select de empresas
    Dado a abertura do "<navegador>"
    Quando insiro o usuario: "<usuario>"
    E acesso o portal GDU
    Entao valido se as empresas inativadas nao estao sendo exibidas no campo select da tela BU
    Exemplos:
      | navegador | usuario   |
      | chrome    | ems52346  |