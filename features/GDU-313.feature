#language: pt
Funcionalidade: GDU-313: Transferir colaborador
  Eu como tester devo validar as regras de negocio e criterios de aceite dessa tela.

  @All @GDU-313 @313-01
  Esquema do Cenario: Validar que seja exibido somente as linhas liderada pelo usuario
    Dado a abertura do "<navegador>"
    Quando insiro o usuario: "<usuario>"
    E acesso o portal GDU
    E acesso a tela: "Transferir colaborador"
    Entao valido que o usuario: "<usuario>" so pode ter acesso as linhas: "<linhas>"
  Exemplos:
    | navegador | usuario   | linhas          |
    | chrome    | 11300000  | Saude, Clinica  |




#Validar que os setores exibidos são abaixo dele
#Validar que os setores a ser promovido sempre são um nível a mais do que o selecionado
#validar que ao selecionar um setor ocupado, esse setor fique para transferência
#Validar que o botão finalizar transferência fique somente habilitado caso tenha selecionado um setor vago
#Validar transferencia realizada com sucesso