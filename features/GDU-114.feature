#language: pt
Funcionalidade: GDU-114: Cadastro de endereços de usuario
  Eu como tester devo validar as regras de negocio e criterios de aceite dessa tela.

  @All @GDU-114 @114-01
  Esquema do Cenario: Editar endereco com sucesso
    Dado a abertura do "<navegador>"
    Quando insiro o usuario: "<usuario>"
    E acesso o portal GDU
    E acesso a tela: "Endereços"
    E acesso a aba: Meus Endereços
    E clico no primeiro endereco cadastrado
    E edito os campos: "<cep>", "<logradouro>", "<bairro>", "<cidade>", "<estado>", "<numero>", "<complemento>", "<residencial>", "<amostra>"
    Entao valido se a mensagem: "Endereço atualizado" foi exibida com sucesso
    Exemplos:
      | navegador | usuario   | cep       | logradouro | bairro       | cidade        | estado          | numero | complemento | residencial | amostra |
      | chrome    | 11200000  | 00000000  | Rua Teste  | Bairro Teste | Cidade Teste  | SP - São Paulo  | 250    | Casa Teste  | false       | false   |

  @All @GDU-114 @114-02
  Esquema do Cenario: Criar novo endereco com sucesso
    Dado a abertura do "<navegador>"
    Quando insiro o usuario: "<usuario>"
    E acesso o portal GDU
    E acesso a tela: "Endereços"
    E edito os campos: "<cep>", "<logradouro>", "<bairro>", "<cidade>", "<estado>", "<numero>", "<complemento>", "<residencial>", "<amostra>"
    Entao valido se a mensagem: "Endereço registrado com sucesso!" foi exibida com sucesso
    Exemplos:
      | navegador | usuario   | cep       | logradouro | bairro       | cidade        | estado          | numero | complemento | residencial | amostra |
      | chrome    | 11200000  | 03583010  |            |              |               |                 | 777    | Casa        | false       | false   |

  @All @GDU-114 @114-03
  Esquema do Cenario: Validar que o botao avancar fique habilitado somente com 1 endereco com as opcoes Residencial e Amostra marcado
    Dado a abertura do "<navegador>"
    Quando insiro o usuario: "<usuario>"
    E acesso o portal GDU
    E verifico se o botao avancar esta desabilitado
    E edito os campos: "<cep>", "<logradouro>", "<bairro>", "<cidade>", "<estado>", "<numero>", "<complemento>", "<residencial>", "<amostra>"
    Entao valido se a mensagem: "Endereço registrado com sucesso!" foi exibida com sucesso
    Entao valido se o botao avancar ficou habilitado com sucesso
    Exemplos:
      | navegador | usuario   | cep       | logradouro | bairro       | cidade        | estado          | numero | complemento | residencial | amostra |
      | chrome    | 11240300  | 00000100  | Rua Teste  | Bairro Teste | Cidade Teste  | SP - São Paulo  | 250    | Casa Teste  | true        | true    |

  @All @GDU-114 @114-04
  Esquema do Cenario: Validar que o botao avancar fique habilitado somente com 1 endereco com a opcao Residencial marcado e outro endereco com a opcao de amostra
    Dado a abertura do "<navegador>"
    Quando insiro o usuario: "<usuario>"
    E acesso o portal GDU
    E verifico se o botao avancar esta desabilitado
    E edito os campos: "<cep>", "<logradouro>", "<bairro>", "<cidade>", "<estado>", "<numero>", "<complemento>", "<residencial>", "<amostra>"
    Entao valido se a mensagem: "Endereço registrado com sucesso!" foi exibida com sucesso
    E edito os campos: "<cep2>", "<logradouro>", "<bairro>", "<cidade>", "<estado>", "<numero2>", "<complemento>", "<residencial2>", "<amostra2>"
    Entao valido se a mensagem: "Endereço registrado com sucesso!" foi exibida com sucesso
    Entao valido se o botao avancar ficou habilitado com sucesso
    Exemplos:
      | navegador | usuario   | cep       | cep2      | logradouro | bairro       | cidade        | estado          | numero | numero2  | complemento | residencial | amostra | residencial2 | amostra2 |
      |  chrome   | 11220300  | 00000100  | 00000100  | Rua Teste  | Bairro Teste | Cidade Teste  | SP - São Paulo  | 250    | 100      |Casa Teste   | true        | false   | false        | true     |

  @All @GDU-114 @114-05
  Esquema do Cenario: Validar que ao criar um novo endereco residencial o anterior e sobrescrito com o novo endereco
    Dado a abertura do "<navegador>"
    Quando insiro o usuario: "<usuario>"
    E acesso o portal GDU
    E acesso a aba: Meus Endereços
    Entao verifico se existe um endereco ja com a opcao residencial marcada
    E acesso a aba: Cadastrar
    E edito os campos: "<cep>", "<logradouro>", "<bairro>", "<cidade>", "<estado>", "<numero>", "<complemento>", "<residencial>", "<amostra>"
    E acesso a aba: Meus Endereços
    Entao valido se o novo endereco criado ficou com a opcao marcada e o endereco antigo sem a opcao residencial marcada
    Exemplos:
      | navegador | usuario   | cep       | logradouro | bairro       | cidade        | estado          | numero | complemento | residencial | amostra |
      |  chrome   | 11240300  | 00000100  | Rua Teste  | Bairro Teste | Cidade Teste  | SP - São Paulo  | 250    |Casa Teste   | true        | false   |
