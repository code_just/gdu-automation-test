#language: pt
Funcionalidade: GDU-361: Alocar colaborador
  Eu como tester devo validar as regras de negocio e criterios de aceite dessa tela.

  @All @GDU-361 @361-01
  Esquema do Cenario: Alocar colaborador com sucesso
    Dado a abertura do "<navegador>"
    Quando acesso com "<usuario>" para validar acesso negado ao GDU
    E realizo os procedimentos com o sysadmin de alocar colaborador com as informacoes: "<setor>", "<usuario>"
    Entao valido se a mensagem: "Usuário registrado" foi exibida com sucesso
    Entao valido se o usuario: "<setor>", esta com acesso ao portal GDU
    Exemplos:
      | navegador | setor     | usuario   |
      | chrome    | 11220108  | 11220108  |