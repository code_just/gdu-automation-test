#language: pt
Funcionalidade: GDU-112: Cadastro de dados pessoais de colaborador
  Eu como tester devo validar as regras de negocio e criterios de aceite dessa tela.

  @All @GDU-112 @112-01
  Esquema do Cenario: Validar acesso a tela cadastro de dados pessoais no primeiro acesso do usuario
    Dado a abertura do "<navegador>"
    Quando insiro o usuario: "<usuario>"
    E acesso o portal GDU
    Entao valido se o icone de step by step esta sendo exibido com sucesso
    Exemplos:
      | navegador | usuario   |
      |  chrome   | 11250200  |

  @All @GDU-112 @112-02
  Esquema do Cenario: Validar usario ja cadastrado nao acesse a tela de primeiro acesso
    Dado a abertura do "<navegador>"
    Quando insiro o usuario: "<usuario>"
    E acesso o portal GDU
    Entao valido se o icone de step by step nao esta sendo exibido com sucesso
    Exemplos:
      | navegador | usuario   |
      |  chrome   | 11230000  |
      |  chrome   | 11220100  |

  @All @GDU-112 @112-03
  Esquema do Cenario: Validar que os campos: Nome, Matricula, CPF e idRede sejam carregados com as informações esperadas e estejam desabilitados para edição
    Dado a abertura do "<navegador>"
    Quando insiro o usuario: "<usuario>"
    E acesso o portal GDU
    Entao deparar se os campos: "<nome>", "<matricula>", "<cpf>" e "<idRede>" estao com as informacoes esperadas e desabiltados para edicao
    Exemplos:
      | navegador | usuario   | nome                          | matricula | cpf         | idRede  |
      |  chrome   | 11230000  | RODRIGO MEDEIROS DE CARVALHO  | 47616     | 01102208469 | 47616   |
      |  chrome   | 11220100  | ANA CLAUDIA LAGES VALDAMBRINI | 315429    | 36870217858 | 46095   |

  @All @GDU-112 @112-04
    Esquema do Cenario: Editar cadastro pessoal com sucesso
    Dado a abertura do "<navegador>"
    Quando insiro o usuario: "<usuario>"
    E acesso o portal GDU
    E edito os campos: "<dataNascimento>", "<apelido>", "<estadoCivil>", "<sexo>", "<numeroCamisa>", "<fumante>", "<pcd>"
    Entao valido se a mensagem: "Dados salvos com sucesso!" foi exibida com sucesso
    Exemplos:
      |navegador  | usuario   | dataNascimento  | apelido              | estadoCivil    | sexo        | numeroCamisa  | fumante | pcd   |
      | chrome    | 11230000  | 23071996        | Teste Automatizado 1 | Casado (a)     | Masculino   | 2             | true    | false |
      | chrome    | 11230000  | 05031998        | Teste Automatizado 2 | Separado (a)   | Feminino    | 3             | false   | true  |
      | chrome    | 11230000  | 07081975        | Teste Automatizado 3 | Divorciado (a) | Outros      | 4             | true    | false |
      | chrome    | 11230000  | 16061960        | Teste Automatizado 4 | Viúvo (a)      | Masculino   | 2             | false   | true  |
      | chrome    | 11230000  | 12081968        | Teste Automatizado 5 | Solteiro (a)   | Feminino    | 1             | false   | true  |

########################################################################################################################
########################################################################################################################
######## FUTUROS TESTES:                                                                                        ########
########                  VALIDAR EDIÇÃO NO BANCO                                                               ########
########                  REALIZAR QUERY NO BANCO PARA REUTILIZAR MASSA - CADASTRO PESSOAL PRIMEIRO ACESSO      ########
########################################################################################################################
########################################################################################################################