#language: pt
Funcionalidade: GDU-322: Bloquear colaborador
  Eu como tester devo validar as regras de negocio e criterios de aceite dessa tela.

  @All @GDU-322 @322-01
  Esquema do Cenario: Desligar colaborador com sucesso
    Dado a abertura do "<navegador>"
    Quando valido se o usuario: "<setor>", esta com acesso ao portal GDU para conferir bloqueio
    E  acesso a tela de bloquear colaborador com o sysadmin
    E seleciono o colaborador da Linha: "<linha>", setor: "<setor>" para "Desligamento" e agendo para hoje
    Entao valido se a mensagem: "Operação agendado com sucesso!" foi exibida com sucesso
    E confirmo "Desligamento" antes do horario marcado
    Entao acesso com "<setor>" para validar acesso negado ao GDU para conferir bloqueio
    Exemplos:
      | navegador | setor     | linha   |
      | chrome    | 11220108  | Clinica |

  @All @GDU-322 @322-02
  Esquema do Cenario: Afastar colaborador com sucesso
    Dado a abertura do "<navegador>"
    Quando valido se o usuario: "<setor>", esta com acesso ao portal GDU para conferir bloqueio
    E  acesso a tela de bloquear colaborador com o sysadmin
    E seleciono o colaborador da Linha: "<linha>", setor: "<setor>" para "Afastamento" e agendo para hoje
    Entao valido se a mensagem: "Operação agendado com sucesso!" foi exibida com sucesso
    E confirmo "Afastamento" antes do horario marcado
    Entao acesso com "<setor>" para validar acesso negado ao GDU para conferir bloqueio
    Exemplos:
      | navegador | setor     | linha   |
      | chrome    | 11220109  | Clinica |